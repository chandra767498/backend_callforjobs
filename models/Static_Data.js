const mongoose = require("../config/connect");

const Static_Data = mongoose.Schema({
    language_id : { type: Number },
    language_wise_data: [ 
        {
            static_id: { type: Number },
            title: { type: String },
            name: { type: String },
            status: { type: Boolean, default: true}
        }
    ]
}, 
{ 
    timestamps: true,
    collection: 'static_data'
});

module.exports = mongoose.callforjobs.model('static_data', Static_Data);