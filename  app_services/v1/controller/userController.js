let userController = function () {};
const moment = require("moment");
const tzmoment = require('moment-timezone');

const service=require('../services/userService')
const Language = require("../../../models/Language");
const DeviceDraft = require("../../../models/Device_Draft");
const Static_Data = require("../../../models/Static_Data");
const States = require("../../../models/States");
const CommonController = require("../../../helpers/CommonController");
const MessagesController = require("../controller/MessagesController")
const users = require("../../../models/users");
const User_OTPS = require("../../../models/user_otps")
const User_OTP_Tries = require("../../../models/User_Otp_Tries")
const config = require("../../../config/config");

const firebaseApis = require("../../../helpers/firebaseApis");



userController.Generate_User_OTP_Send_Message = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let OTP = await CommonController.Random_OTP_Number();
            let Data = {
                CountryCode: values.country_code,
                PhoneNumber: values.mobile_number,
                OTP: OTP,
                Latest: true,
                Time: new Date()
            }
            let SaveResult = await User_OTPS(Data).save();
            resolve({ success: true, msg: "OTP Sent Successfully" });

            let PhoneNumber = `${values.country_code}${values.mobile_number}`;
            if( process.env.NODE_ENV == "production" ){
                let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, OTP, values.message_id);
            }

            let query = {
                CountryCode: values.country_code,
                PhoneNumber: values.mobile_number,
                OTP: { $ne: OTP }
            };
            let changes = {
                Latest: false
            };
            let UpdatedStatus = await User_OTPS.updateMany(query, changes).lean();

            let draftUpdate = await DeviceDraft.updateOne({ id: values.draft_id }, { $set: { user_name: values.user_name } });
            // let commentUpdate = await Comment.updateMany({ draft_id: values.draft_id }, { $set: { user_name: values.user_name } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

userController.Check_for_OTP_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    CountryCode: values.country_code,
                    PhoneNumber: values.mobile_number,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await User_OTPS.countDocuments(query).lean();
                if (Count <= config.OTP_COUNT) {
                    resolve('Validated Successfully')
                } else {
                    reject({ success: false, extras: { msg: "OTP REQUEST EXCEED TRY AFTER SOME TIME" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

userController.Check_for_User_OTP_Tries_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    CountryCode: values.country_code,
                    PhoneNumber: values.mobile_number,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await User_OTP_Tries.countDocuments(query).lean().exec();
                if (Count <= config.OTP_TRIES_COUNT) {
                    resolve('Validated Successfully');
                } else {
                    reject({ success: false, extras: { msg: "OTP TRIES EXCEED TRY AFTER SOME TIME" } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

userController.Validate_User_OTP = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OTP_Query = {
                    $eq: values.otp
                };
                let query = {
                    CountryCode: values.country_code,
                    PhoneNumber: values.mobile_number,
                    OTP: OTP_Query,
                    Latest: true
                };
                if(process.env.NODE_ENV != "production" && values.otp == "2020"){
                    query.OTP = {
                        $ne: ""
                    }
                }
                let Result = await User_OTPS.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: "INVALID OTP" } });
                    let Data = {
                        CountryCode: values.country_code,
                        PhoneNumber: values.mobile_number,
                        Time: new Date()
                    };
                    let SaveResult = await User_OTP_Tries(Data).save();
                } else {
                    resolve("Validated Successfully");
                    let RemoveTries = await User_OTP_Tries.deleteMany({
                        CountryCode: values.country_code,
                        PhoneNumber: values.mobile_number
                    }).lean();
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

userController.Add_Fetch_User_Profile_Information = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let matchQuery = {
                    country_code: values.country_code,
                    mobile_number: values.mobile_number
                };
                let deviceQuery = { id: values.draft_id };
                let draftData = await DeviceDraft.findOne(deviceQuery).lean();
                 // console.log(Result)
                // return;
                let Result = await users.findOne(matchQuery).lean();
               
                let UserData = {
                    "country_code": values.country_code,
                    "mobile_number": values.mobile_number,
                    // "first_name": draftData.first_name,
                    // "email_id": draftData.email_id,
                    // "profile_pic": draftData.profile_pic
                };
              
                let user_id = 0, login_time = null;
                if( !Result ){
                    login_time = new Date();
                    let id = await CommonController.generateSequenceNumber('writer_id');
                    let referData = await firebaseApis.generateRefLink(id);
                    let data = {
                        "id": id,
                        "country_code": values.country_code,
                        "mobile_number": values.mobile_number,
                        // "user_name": draftData.user_name,
                        // "email_id": draftData.email_id,
                        // "profile_pic": draftData.profile_pic,
                        // "ref_link": referData.short_link,
                        // "ref_long_link": referData.long_link
                    }
                    let user = await users(data).save();
                    user_id = user.id;
                }
                else{
                    user_id = Result.id;
                }
                delete UserData._id;
                let updateChanges = {};
                updateChanges['$set'] = {
                    user_id: user_id,
                    login_time: login_time
                }
                UserData.user_id = user_id;
                await DeviceDraft.findOneAndUpdate(deviceQuery, updateChanges).exec();
                resolve({  success: true, msg: "Successfull Login", data: UserData});

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

userController.Send_otp = async (req,res)=>{
    if(!req.body.mobile_number){
          return res.send({success:false,msg:"mobile number is required"})
    }
    let result = await service.Send_otp(req.body);
    res.send(result)
}



userController.Verify_otp = async (req,res)=>{
    if(!req.body.mobile_number){
          return res.send({success:false,msg:"mobile number is required"})
    }
    let result = await service.Verify_otp(req.body);
    res.send(result)
}
userController.List_All_Languages = async (req, res) => {
    
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {};
                if(process.env.NODE_ENV == "production"){
                    query = {
                        is_test: false
                    };
                }
                let Result = await Language.find(query,{_id:0, is_test: 0}).sort({id:1}).lean().exec();
                resolve({ success: true, Data: Result });
            }
            catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
    
}


userController.Update_User_Language = async (values) => {
    let languageQuery = {
        id: values.language_id,
        status: true
    }
    let Result = await Language.findOne(languageQuery).lean();
    if (Result != null) {
        let query = {
            id: values.draft_id
        };
        let changes = {
            $set: {
                language_id: values.language_id,
                // state_id: 0,
                // dist_id: 0
            }
        };
        let updateDraft = await DeviceDraft.findOneAndUpdate(query, changes);

        let xData = {}
        let SResult = await Static_Data.findOne( { language_id: values.language_id }, { language_wise_data: 1, _id: 0 }).lean();
        if(SResult){
	        SResult.language_wise_data.forEach( (item) => {
	            xData[item.title] = item.name;
	        })
	    }
        // SResult.forEach( (item) => {
        //     let value = item.language_wise_titles.filter( (data) => {
        //         return data.language_id == values.language_id;
        //     })
        //     xData[item.title] = value[0].language_title;
        // })

        let next_screen = "register";
        if( values.language_id == 1 ){
            next_screen = "register";
        }
        else{
            let statesCount = await States.countDocuments( { language_id: values.language_id, status: true } );
            next_screen = "main"
        }
        return { success: true, data: xData, next_screen: next_screen };
    } else {
        return { success: false, msg: "INVALID LANGUAGE" };
    }
}




userController.Get_Static_Data = async (values) => {
    let query = {
        id: values.draft_id
    };
    let changes = {
        $set: {
            static_data_api: 0
        }
    };
    let updateDraft = await DeviceDraft.findOneAndUpdate(query, changes, { new: true });
    let xData = {}
    let SResult = await Static_Data.findOne( { language_id: values.language_id }, { language_wise_data: 1, _id: 0 }).lean();
    if(SResult){
        SResult.language_wise_data.forEach( (item) => {
            xData[item.title] = item.name;
        })
    }

    let next_screen = "main";
    if( values.language_id != 1 && !updateDraft.state_id){
        next_screen = "state"
        let statesCount = await States.countDocuments({ language_id: values.language_id, status: true });
        next_screen = statesCount != 1 ? "state" : "district"
    }

    return { success: true, data: xData, next_screen: next_screen };
}


userController.Register = async (values) => {
    //
    // values.admin_id = null;
    // values.session_id = null;
    // values.id = await CommonController.generateSequenceNumber("employee_id");
    // values.status = true;
    let userObj = {
        // id: await CommonController.generateSequenceNumber("employee_id"),
        // emp_id: values.emp_id,
        first_name: values.first_name,
        last_name: values.last_name,
        email_id: values.email_id,
        mobile_number: values.mobile_number,
        present_location: values.present_location,
        dob: values.dob,
        gender: values.gender,
        select_education: values.select_education,
        current_company: values.current_company,
        job_designation: values.job_designation,
        job_intrested: values.job_intrested,
        select_experience: values.select_experience,
      
    }
    // let users = await users.findOne({ EmailID: values.EmailID }).lean();
    // if (users) {
    //   return { success: false, msg: "email id already exist." };
    // }

    await new users(values).save();
  
    return { success: true, msg: "Added Successfully." };
  };
  

module.exports = userController;