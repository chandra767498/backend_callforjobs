

const request = require('request');
const { resolve } = require('path');

const config = require('../config/config');
// const API_KEY = config.firebase.webApiKey;

// const body = {
//     'longDynamicLink': 'https://anynews1.page.link/?link=https://play.google.com/store/apps/details?id=com.anyNews.anynews'
// }

// request({
//     url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
//     method: 'POST', json: true, body
// }, function (error, response) {
//     if (error) {
//         console.log('Error :', error)
//     } else {
//         if (response && response.statusCode !== 200) {
//             console.log('Error on Request :', response.body.error.message)
//         } else {
//             console.log('Dynamic Link :', response.body);
//         }
//     }
// });

// function generateShareLink(post_id){
//     return new Promise((resolve)=>{
//         try{
//             const body = {
//                 // 'longDynamicLink': 'https://anynews0.page.link/?link=https://play.google.com/store/apps/details?id=com.anyNews.anynews?articleId=' + post_id,
//                 'dynamicLinkInfo': {
//                     'domainUriPrefix': 'https://anynews0.page.link',
//                     'link': 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&articleId=' + post_id  + "&user_id" + 0 + '&type=share&medium=app',
//                     'androidInfo': {
//                         "androidPackageName": "com.anyNews.anynews"
//                     },
//                     // "iosInfo": {
//                     //     "iosBundleId": string,
//                     //     "iosFallbackLink": string,
//                     //     "iosCustomScheme": string,
//                     //     "iosIpadFallbackLink": string,
//                     //     "iosIpadBundleId": string,
//                     //     "iosAppStoreId": string
//                     // },
//                 },
//                 "suffix": {
//                     "option": "UNGUESSABLE"
//                 }
//             }
//             request({
//                 url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
//                 method: 'POST', json: true, body
//             }, function (error, response) {
//                 if (error) {
//                     console.log('Error :', error);
//                     resolve({ link: "https://anynews0.page.link/RRMb" });
//                 } else {
//                     if (response && response.statusCode !== 200) {
//                         console.log('Error on Request :', response.body.error.message);
//                         resolve("https://anynews0.page.link/RRMb");
//                     } else {
//                         console.log('Dynamic Link :', response.body);
//                         resolve(response.body.shortLink);
//                     }
//                 }
//             });
//         }
//         catch(err){
//             console.log(err);
//             resolve("https://anynews0.page.link/RRMb");
//         }
//     })
// }

// function generateShareRefLink(post_id, user_id){
//     return new Promise((resolve)=>{
//         let defaultLink = 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&articleId='+ 0 +'&user_id='+ 0 +'&type=default&medium=app';
//         let deepLink = 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&articleId'+ post_id +'&user_id='+ user_id +'&type=share&medium=app';
//         let shortLink = "https://anynews0.page.link/J9vC";
//         let iosDeepLink = 'https://apps.apple.com/in/app/anynews-regional-news-app/id1543144956?id=anynews&articleId=0&user_id=0&type=default&medium=app';
//         try{
//             const body = {
//                 // 'longDynamicLink': 'https://anynews0.page.link/?link=https://play.google.com/store/apps/details?id=com.anyNews.anynews?articleId=' + post_id,
//                 'dynamicLinkInfo': {
//                     'domainUriPrefix': 'https://anynews0.page.link',
//                     'link': deepLink,
//                     // 'link': 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&user_id=' + user_id + '&type=referral',
//                     'androidInfo': {
//                         "androidPackageName": "com.anyNews.anynews",
//                         "androidFallbackLink": deepLink
//                     },
//                     "iosInfo": {
//                         "iosBundleId": "com.anyNews.anynews.b",
//                         "iosFallbackLink": iosDeepLink,
//                         // "iosAppStoreId": '1543144956',
//                         // "iosCustomScheme": string,
//                         // "iosIpadFallbackLink": string,
//                         // "iosIpadBundleId": string
//                     },
//                     "socialMetaTagInfo": {
//                         "socialTitle": "Share & Earn",
//                         "socialDescription": "",
//                         "socialImageLink": "https://newsany.s3-us-west-2.amazonaws.com/logo.png"
//                     }
//                 },
//                 "suffix": {
//                     "option": "UNGUESSABLE"
//                 }
//             }
//             request({
//                 url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
//                 method: 'POST', json: true, body
//             }, function (error, response) {
//                 if (error) {
//                     console.log('Error :', error);
//                     resolve({ short_link: shortLink, long_link: defaultLink });
//                 } else {
//                     if (response && response.statusCode !== 200) {
//                         console.log('Error on Request :', response.body.error.message);
//                         resolve({ short_link: shortLink, long_link: defaultLink });
//                     } else {
//                         console.log('Dynamic Link :', response.body);
//                         resolve({ short_link: response.body.shortLink, long_link: deepLink });
//                     }
//                 }
//             });
//         }
//         catch(err){
//             console.log(err);
//             resolve({ short_link: shortLink, long_link: defaultLink });
//         }
//     })
// }

function generateRefLink(user_id){
    return new Promise((resolve)=>{
        let defaultLink = 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&user_id='+ 0 +'&type=default&medium=app';
        let deepLink = 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&user_id='+ user_id +'&type=referral&medium=app';
        let shortLink = "https://anynews0.page.link/J9vC";
        let iosDeepLink = 'https://apps.apple.com/in/app/anynews-regional-news-app/id1543144956?id=anynews&user_id=0&type=default&medium=app';
        try{
            const body = {
                // 'longDynamicLink': 'https://anynews0.page.link/?link=https://play.google.com/store/apps/details?id=com.anyNews.anynews?articleId=' + post_id,
                'dynamicLinkInfo': {
                    'domainUriPrefix': 'https://anynews0.page.link',
                    'link': deepLink,
                    // 'link': 'https://play.google.com/store/apps/details?id=com.anyNews.anynews&user_id=' + user_id + '&type=referral',
                    'androidInfo': {
                        "androidPackageName": "com.anyNews.anynews",
                        "androidFallbackLink": deepLink
                    },
                    "iosInfo": {
                        "iosBundleId": "com.anyNews.anynews.b",
                        "iosFallbackLink": iosDeepLink,
                        // "iosAppStoreId": '1543144956',
                        // "iosCustomScheme": string,
                        // "iosIpadFallbackLink": string,
                        // "iosIpadBundleId": string
                    },
                    "socialMetaTagInfo": {
                        "socialTitle": "Refer & Earn with Anynews",
                        "socialDescription": "The Best Regional Short News App",
                        "socialImageLink": "https://newsany.s3-us-west-2.amazonaws.com/logo.png"
                    }
                },
                "suffix": {
                    "option": "UNGUESSABLE"
                }
            }
            request({
                url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
                method: 'POST', json: true, body
            }, function (error, response) {
                if (error) {
                    console.log('Error :', error);
                    resolve({ short_link: shortLink, long_link: defaultLink });
                } else {
                    if (response && response.statusCode !== 200) {
                        console.log('Error on Request :', response.body.error.message);
                        resolve({ short_link: shortLink, long_link: defaultLink });
                    } else {
                        console.log('Dynamic Link :', response.body);
                        resolve({ short_link: response.body.shortLink, long_link: deepLink });
                    }
                }
            });
        }
        
        catch(err){
            console.log(err);
            resolve({ short_link: shortLink, long_link: defaultLink });
        }
    })
}

module.exports = {
    // generateShareLink,
    generateRefLink,
    // generateShareRefLink
}