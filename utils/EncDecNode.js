const aes_node = module.exports
// var CryptoJS = require('crypto-js')

// var KEY_SIZE = 50
// var iv = '0000000000000000'  // '83963b0fedccb4ce' // CryptoJS.lib.WordArray.random(8).toString();

// // Encryption using AES CBC (128-bits)
// aes_node.encrypt = function (plaintext, key) {
//   try {
//     key = (key.length > KEY_SIZE) ? key.substr(0, KEY_SIZE) : key
//     var encrypted = CryptoJS.AES.encrypt(
//       plaintext,
//       CryptoJS.enc.Utf8.parse(key), {
//         mode: CryptoJS.mode.CBC,
//         iv: CryptoJS.enc.Utf8.parse(iv),
//         // PKCS#7 with 8-byte block size
//         padding: CryptoJS.pad.Pkcs7
//       }
//     )
//     return encrypted.ciphertext.toString(CryptoJS.enc.Base64)
//   }
//   catch (error) {
//     console.log('Encryption exception in encrypt(): ' + error.message)
//     return null
//   }
// }

// let v = aes_node.encrypt("hello", "12345678900987654321123456789009");
// console.log(v)


// // Decryption using AES CBC (128-bits)
// aes_node.decrypt = function (ciphertext, key) {
//   try {
//     key = (key.length > KEY_SIZE) ? key.substr(0, KEY_SIZE) : key
//     var decrypted = CryptoJS.AES.decrypt(
//       ciphertext,
//       CryptoJS.enc.Utf8.parse(key), {
//         mode: CryptoJS.mode.CBC,
//         iv: CryptoJS.enc.Utf8.parse(iv),
//         // PKCS#7 with 8-byte block size
//         padding: CryptoJS.pad.Pkcs7
//       }
//     )
//     return decrypted.toString(CryptoJS.enc.Utf8)
//   } catch (error) {
//     console.log('Encryption exception in decrypt(): ' + error.message)
//     return null
//   }
// }




/** ************************************** FOR TEST **********************************************/

// var enc = aes_node.encrypt('hai helloooo', 'thisisasamplepassphraseforencoding');
// console.log(enc);
// var dec = aes_node.decrypt(enc, 'thisisasamplepassphraseforencoding');
// console.log(dec);

// let v = "hW/bzYBUmdnkq8ZX4z3aymplItRYKfRqhiIvhA5RR6DOyTk8sFV8Y8MDFWRvf/OLUZADmIVObFUnQ4P4FRgrAArTgEUbDFuQfSrr9V6V1jp3kiFKLmXgaP23RVt5RSiuD8TDwbFbwu9nTyW2huLVKTAwGWuAgdRFJWlD7hHMyT/e8JrIS/M1MAeX9Y5fWyWP5KCK9Wn0FXoo9Zz6m6ikPAB3ssAtkRO+h8ZEb7bMRUrmkJyUuChKpI/whfp1MTqgeGYSW13bBK1Vlmdb59xtcBXXVYc3xXawppfYVfjuyrdMEYm08G5v931mHUmIKlxcRC9roaw92jsy92gtZB5qQCJjbyHYG63RjiVcfaXujUNXe2sViQuA32HDT3pOTxeX+4JPAZgFpaaBnsXVtmbI3lymrNeV3krlrnRPaCgT6xGC7nmxSo3eljWck9JFiEiUQ7HEeuEa7Wa2UcIzbjxwI7B7By6vtZ5ooOBIemDi7e3wzAKVeRbofqaEvYIWuKQy6q/KgvxnstcJUTNB3Jahq8/BDcthJfh55hXVoo/66UtyetsNMdnqzkxf+3o4lC2VdAnZC4hVo3Kte2fAhq+ksl/1FHfkMNms/Tz9OlMtoLvgIeyE5f0pCVqerS3ZMcnlPYr7oc3huyBZXwuKbI3QlohX3tR+yViYugYw8mnK9yt9R6b7GO+IWJ90yU+2RzNc"
// let result = aes_node.decrypt(v,"ABCDEFGHIJKLMNOPQRSWXYZ0123456789")
// console.log(result)

// const crypto = require('crypto');
// const algorithm = 'aes-256-cbc';
// const key =  "12345678900987654321123456789009" //crypto.randomBytes(32);
// const iv = Buffer.from("0000000000000000", 'utf8'); // crypto.randomBytes(16);  //"0000000000000000"

// function encrypt(text) {
//   console.log(iv)
//  let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
//  let encrypted = cipher.update(text);
//  encrypted = Buffer.concat([encrypted, cipher.final()]);
//  return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
// }

// function decrypt(text) {
//  let iv = Buffer.from(text.iv, 'hex');
//  let encryptedText = Buffer.from(text.encryptedData, 'hex');
//  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
//  let decrypted = decipher.update(encryptedText);
//  decrypted = Buffer.concat([decrypted, decipher.final()]);
//  return decrypted.toString();
// }

// var hw = encrypt("hello")
// console.log(hw)
// console.log(decrypt(hw))




// const crypto = require('crypto');
// const INITIALIZATION_VECTOR = '0000000000000000';

// class Crypt {
//     static decrypt128(data, key) {
//         const cipher = crypto.createDecipheriv('aes-128-cbc', Buffer.from(key, 'hex'), Buffer.from(INITIALIZATION_VECTOR));
//         return cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
//     }
//     static encrypt128(data, key) {
//         const cipher = crypto.createCipheriv("aes-128-cbc", Buffer.from(key, "hex"), Buffer.from(INITIALIZATION_VECTOR));
//         return cipher.update(data, "utf8", "hex") + cipher.final("hex");
//     };
// }

// const key = "12345678900987654321123456789009";
// const data = 'hello';
// const cipher = Crypt.encrypt128(data, key);
// const decipher = Crypt.decrypt128(cipher, key);

// console.log(cipher);
// console.log(decipher);




const crypto = require('crypto');
const { gzip, ungzip } = require('node-gzip');

const key = require("../credentials/testMail").secretKey;

aes_node.encrypt = async function (data) {
  var cc = crypto.createCipher('aes-128-ecb', new Buffer.from(key));
  var encrypted = Buffer.concat([cc.update(data, 'utf8'), cc.final()]).toString('base64');
  // let buff = Buffer.from(data, 'utf-8');
  // let base64 = buff.toString('base64');
  // let response = await gzip(encrypted);
  return encrypted;
}

aes_node.decrypt = async function (data) {
  // let ciphertext = await ungzip(data);
  var cc = crypto.createDecipher('aes-128-ecb', new Buffer.from(key));
  var decrypted = Buffer.concat([cc.update(data, 'base64'), cc.final()]).toString('utf8');
  
  // let ciphertext = await ungzip(data);
  // let buff = Buffer.from(ciphertext, 'base64');
  // let plaintext = buff.toString('utf-8');
  return JSON.parse(decrypted);
}