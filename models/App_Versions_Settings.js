const mongoose = require("../config/connect");

const App_Versions_Settings = mongoose.Schema({
    Android_Version: { type: Number, default: 1 },
    IOS_Version: { type: Number, default: 1 }
}, { collection: "App_Versions_Settings" });

module.exports = mongoose.callforjobs.model('App_Versions_Settings', App_Versions_Settings);