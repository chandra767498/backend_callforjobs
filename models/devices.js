const mongoose = require("mongoose");

const draftSchema = mongoose.Schema({
    id: { type: Number, unique: true },
    device_id: { type: String, default: "" },
    device_name: { type: String, default: "" },
    device_type: { type: Number, default: 1 },
    app_version: { type: Number, default: 1 },
    ip_address: { type: String, default: "" },
    fcm_token: { type: String, default: "" },
    language_id: { type: Number },
    ad_id: { type: String, default: "" },
    user_id: { type: Number, default: 0 },
    user_name: { type: String, default: "Guest" },
    email_id: { type: String, default: "" },
    mobile_number: { type: String, default: "" },
    profile_pic: { type: String, default: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp" },
    original_profile_pic: { type: String, default: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp" },
    login_time: { type: Date },
    install_time: { type: Date },
    reinstalls: { type: Number, default: 0 },
    status: { type: Boolean, default: true },
    // notification_flag: { type: Boolean, default: true },
    // location_flag: { type: Boolean, default: false },
    // longitude: { type: String },
    // latitude: { type: String },
    static_data_api: { type: Number, default: 0 },
    update_menu_status: { type: Number, default: 1 },  //1-updated 2-manual update 3-force update
    language_screen_status: { type: Boolean, default: false },
    refer_status: { type: Boolean, default: false },
    active_status: { type: Boolean, default: true },
    uninstall_time: { type: Date, default: new Date() },
    reinstall_time: { type: Date },
    install_source: { type: String, default: "google" },    //google, referral, facebook, instagram, twitter
    // country: { type: String, default: "" },
    // city: { type: String, default: "" },
    // pincode: { type: String, default: "" }
},
{
    timestamps: true,
    collection: "device_drafts" 
});

draftSchema.index({ device_id: 1 });
draftSchema.index({ id: 1 });
draftSchema.index({ user_id: 1 });

module.exports = mongoose.model('device_drafts', draftSchema);