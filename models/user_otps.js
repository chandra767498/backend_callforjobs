const mongoose = require("../config/connect");

const User_OTPS = mongoose.Schema({
    CountryCode: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    OTP: { type: Number, default: 0 },
    Latest: { type: Boolean, default: true },
    Time: { type: Date, default: null }
}, { collection: "User_OTPS" });

module.exports = mongoose.callforjobs.model('User_OTPS', User_OTPS);

