const helper = require("../../../helpers/helper");
const user_otps = require("../../../models/user_otps");
const userMOdel = require("../../../models/users");
const Language = require("../../../models/Language");

var userService = {};

userService.Send_otp = async (values) => {
  let otp = await helper.Random_OTP_Number();
  try {
    let Data = {
      country_code: values.country_code || "+91",
      mobile_number: values.mobile_number,
      otp: otp,
      latest: true,
    };
    let Result = await user_otps(Data).save();

    let MessageID = values.message_id || "1234";
    let phoneNumber = Data.country_code + Data.mobile_number;
    let message = `Welcome to CallForJobs, Your OTP for login is ${otp}. Message-ID : ${MessageID}. Do not share this OTP with others for security reasons.`;
    let OTPStatus = await helper.Send_SMS(phoneNumber, message);
    let query = {
      country_code: values.country_code,
      mobile_number: values.mobile_number,
      otp: { $ne: otp },
    };
    let changes = {
      latest: false,
    };
    let UpdatedStatus = await user_otps.updateMany(query, changes).lean();
    if (OTPStatus) {
      return { success: true, msg: "Otp sent successfully" };
    }
    return { success: false, msg: "Something went wrong" };
  } catch (err) {
    console.log(err);
    return { success: false, msg: "Internal server error" };
  }
};

// userService.Verify_otp = async (values)=>{
//     try{
//         let OTP_Query = {
//             $eq: values.otp
//         };
//         let query = {
//             country_code: values.country_code,
//             mobile_number: values.mobile_number,
//             otp: OTP_Query,
//             latest: true
//         };
//         let Result=await user_otps.findOne(query).lean();
//         if(Result){
//             let next_screen = "info";
//             let userData = await userMOdel.findOne({mobile_number:values.mobile_number})
//             if( userData && !userData.language_id ){
//                 next_screen = "language";
//             }
//             else if(userData && userData.status){
//                 next_screen="main"
//             }
//             else if( !userData ){
//                 await userMOdel({ mobile_number: values.mobile_number, status: false }).save();
//             }

//             return { success: true, msg: "verified successfully", next_screen: next_screen };
//         }
//         return { success: false, msg: "INVALID OTP" };

//     }
//     catch(err){
//         return {success:true,msg:"Internal server error"}
//     }
// }
userService.List_All_Languages = async (values) => {
  let Result = await Language.find({ active: true });
  return { success: true, data: Result };
};

module.exports = userService;
