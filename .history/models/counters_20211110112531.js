const mongoose = require("../config/connect");

const schema = mongoose.Schema({
    collection_name: { type: String, default: "" },
    seq_no: { type: Number, default: 0 }
})

module.exports = mongoose.model('counters', schema);