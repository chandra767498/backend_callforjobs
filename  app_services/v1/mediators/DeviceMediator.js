let DeviceMediator = function () { };

const DeviceController = require("../controller/DeviceController");
const CommonController = require("../../../helpers/CommonController");


DeviceMediator.Splash_Screen_Animation = async (req, res) => {
    try {
        if (
            req.body.DeviceID
        ) {
            let Result = await DeviceController.Splash_Screen_Animation(req.body);
            res.json(Result);
        }
        else {
            throw { success: false, extras: { msg: "Enter All Tags" } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

module.exports = DeviceMediator;