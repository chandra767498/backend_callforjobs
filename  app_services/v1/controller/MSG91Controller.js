
let MSG91Controller = function () { };

const MSG91 = require("msg91");
const axios = require("axios");

const config = require("../../../config/config");
const CommonController = require("../../../helpers/CommonController");

let msg91 = MSG91(config.msg91.authkey, config.msg91.sender_id, config.msg91.route_no);

MSG91Controller.Send_SMS = (PhoneNumber, Message) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let MessageSent = await msg91.send(PhoneNumber, Message);
                resolve("Message Sent Successfully");
            } catch (error) {
                console.error("MSG91 error----->", error);
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

MSG91Controller.Get_MSG91_BALANCE = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let request_options = {
                    url: '/balance.php',
                    method: 'get',
                    baseURL: config.msg91.host,
                    params: {
                        type: config.msg91.route_no,
                        authkey: config.msg91.authkey
                    }
                };
                let Response = await axios(request_options);
                if (Response.status == 200) {
                    let Data = Response.data;
                    resolve(Data);
                } else if (Response.status == 400) {
                    console.error("MSG91_ERROR------->", Response);
                    reject({ success: false, extras: { msg: "MSG91 ERROR" } });
                } else if (Response.status == 401) {
                    console.error("MSG91_ERROR------->", Response);
                    reject({ success: false, extras: { msg: "MSG91 ERROR" } });
                }
            } catch (error) {
                console.error("MSG91_ERROR Error-->", error);
                reject({ success: false, extras: { msg: "MSG91 ERROR" } });
            }
        });
    });
}


module.exports = MSG91Controller;