const mongoose = require("../config/connect");

const companies = mongoose.Schema({
    company_id: { type: Number, default: 0 },
  
    SNo: { type: Number, default: 0 },
    id: { type: Number, unique: true },
    name: { type: String, default: "" },
    category: { type: String, default: "" },
    mobile: { type: String, default: "" },
    url: { type: String, default: "" },
    status: { type: Boolean, default: true }

}, 
{ 
    timestamps: true,
    collection: "companies" 
});

module.exports = mongoose.callforjobs.model('companies', companies);