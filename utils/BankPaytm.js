var checksum = require('./checksum');
var request = require('request');

var samarray = new Array();
let date = new Date(Date.now() + 19800000).toISOString().split('T')[0];
samarray = {
    "mid": "Way2Ne87370173246026",
    "orderId": '2019orde758jkj33k32162',
    "beneficiaryAccount": '919899996782',
    "beneficiaryIFSC": 'PYTM0123456',
    "amount": '1',
    "subwalletGuid": "9a6916e5-cfd0-11ea-b4b3-fa163e429e83",
    "purpose": "OTHERS",
    "date": date
};
var finalstring = JSON.stringify(samarray);
// console.log("samarray : ", finalstring)
checksum.genchecksumbystring(finalstring, "st_9wD9s7g8%xkTz", function (err, result) {
    console.log("CHEKSUM :: ", result);
    request({
        url: 'https://dashboard.paytm.com/bpay/api/v1/disburse/order/bank',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'x-mid': 'Way2Ne87370173246026',
            'x-checksum': result
        },
        body: finalstring //Set the body as a string

    }, function (error, response, body) {
        if (error) {
            console.log('error', error);
            // callback(null);
        } else {
            console.log(response.statusCode, body);
            let resObj = JSON.parse(body);
            // callback(resObj);
        }
    });
});