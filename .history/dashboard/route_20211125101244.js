const express = require("express");
const router = express.Router();
const userController = require("./controller/userController");
const AdminMediator = require("./mediators/AdminMediator");

//login
router.post("/Login", AdminMediator.Login);

router.post("/logout", AdminMediator.Logout);

router.post("/Create_Admin_User", AdminMediator.Create_Admin_User);

//language
// router.post("/Create_language", userController.Create_language);

router.post("/Add_Language", AdminMediator.Add_Language);

router.post("/List_All_Languages", AdminMediator.List_All_Languages);

router.post("/Add_Static_Language", AdminMediator.Add_Static_Language);

// router.post("/Admin_Fetch_Static_Data", AdminMediator.Admin_Fetch_Static_Data);

router.post('/update_language', AdminMediator.Update_Language);


//jobs
router.post("/Add_job_category", AdminMediator.Add_job_category);

router.post('/List_All_job_Category', AdminMediator.List_All_job_Category);

router.post('/Active_Inactive_job_Category', AdminMediator.Active_Inactive_job_Category);


router.post("/Add_company", AdminMediator.Add_company);

router.post('/List_All_Companies', AdminMediator.List_All_Companies);


//posting
router.post('/add_job_posting', AdminMediator.Add_Job_Posting);

router.post('/view_states', AdminMediator.view_states);

router.post('/view_cities', AdminMediator.view_cities);

router.post('/Get_Job_Posting_Details', AdminMediator.Get_Job_Posting_Details);


//employees
router.post('/List_All_Employees', AdminMediator.List_All_Employees);

router.post("/edit_employee", AdminMediator.Edit_Employee);

router.post('/Active_Inactive_Employee', AdminMediator.Active_Inactive_Employee);

module.exports = router; 
