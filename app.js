const express=require('express');
const app=express();
const morgan = require('morgan');
const chalk = require("chalk");
const cors =  require("cors");

const mongoose=require('mongoose')
mongoose.connect('mongodb://localhost:27017/callforjobs')
.then(()=>console.log(`Mongo DB connected`))
.catch(()=>console.log(`Mongo DB not connected`));

//Express middleware
app.use(express.json({limit: "50mb"}));

app.use(morgan(function (tokens, req, res) {
    let method = tokens.method(req, res);
    let url = tokens.url(req, res);
    let resptime = `${tokens['response-time'](req, res)} ms`;
    return `${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)}`;
}))

const userRouter= require("./routes/user");
const queryRouter= require('./routes/querie');
const v1Route=require('./ app_services/v1/route')
const dashboardRoute=require('./dashboard/route')
//Router middleware
// app.use('/users',userRouter);
// app.use('/queries',queryRouter);
app.use('/app/v1',v1Route);
app.use('/dashboard',dashboardRoute);
// app.get('/',(req,res)=>{
//     console.log('test')
// });
const port=process.env.PORT||3005;
app.listen(port,()=>console.log(`Server started on ${port}`))