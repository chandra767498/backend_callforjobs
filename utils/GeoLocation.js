geoLocation = module.exports;

const toCsv = require("to-csv");
const ipapi = require('ipapi.co');
const request = require("request");

const accessToken = require("../credentials/testMail").locationToken;
const mailSender = require('../helpers/mailSender');

const drafts = require("../models/Device_Draft");

geoLocation.getLocationFromPoints = async function(latitude, longitude){
    let result = await new Promise((resolve) => {
        let option = {
            url: 'https://us1.locationiq.com/v1/reverse.php?key='+ accessToken +'&lat='+ latitude +'&lon='+ longitude +'&format=json',
            method: "GET"
        }
        request(option, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve(false);
            }
            else {
                console.log(response.statusCode, body);
                let resObj = JSON.parse(body);
                resolve(resObj);
            }
        })
    })
    return result;
}

// async function add(){
//     let v = await geoLocation.getLocationFromPoints("15.994926475893777", "80.47726977105293");
//     console.log(v.address.state_district, v.address.state);
// }

// add();

var failed = [];
var success = [];

async function getLocation(ip){
    return new Promise((resolve)=> {
        setTimeout(()=>{
            ipapi.location((data)=>{
                if( data.region ){
                    console.log(ip, " : ", data.region, " -> ", data.city);
                    success.push({ "state": data.region, "city": data.city });
                }
                else{
                    failed.push(ip);
                }
                resolve("done");
            }, ip)
        }, 1000);
    })
}

async function getLocationFromIP(data){
    failed = [];
    if( data.length ){
        for( let ip of data ){
            await getLocation(ip);
        }
        return getLocationFromIP(failed);
    }
    else{
        let content = toCsv(success);
        let subject = "locations";
        let filename = "locations.csv";
        let attachments = [{ filename: filename, content: content }];

        let mailResult = await mailSender.sendMail([ "bullirajugbr@gmail.com", "ravi105763@gmail.com" ], [], [], subject, "", attachments, "");
        console.log(mailResult);
        return;
    }
}

async function getData(){
    let devices = await drafts.find({ language_id: 1, createdAt: { $gte: new Date("2021-07-29T18:30") } }).lean();
    let data = [];
    for(let device of devices){
        data.push(device.ip_address);
    }
    getLocationFromIP(data);
}

// getData();