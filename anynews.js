const express = require("express");
const HoganExpress = require("hogan-express");
const bodyParser = require("body-parser");
const morgan = require('morgan');
const CookieParser = require("cookie-parser");
const chalk = require("chalk");
const Rollbar = require("rollbar");

require('newrelic');

require('events').EventEmitter.defaultMaxListeners = 20;

let rollbar = new Rollbar({
  accessToken: 'cbf9ba0d65f54551855a6b12d13b192e',
  captureUncaught: true,
  captureUnhandledRejections: true
});

const config = require("./config/config");
const scheduleJobs = require('./scheduleJobs');
const { createFolders } = require("./helpers/seeds");

const oldDashboardRouter = require('./old_dashboard/route');
const dashboardRouter = require('./dashboard/route');
const uploadRouter = require('./dashboard/upload_route');
const appV1Router = require('./app_services/v1/route');
const appV2Router = require('./app_services/v2/route');
const appV3Router = require('./app_services/v3/route');
const appV2_1Router = require('./app_services/v2.1/route');  //ios
const appV2_2Router = require('./app_services/v2.2/route');  //android
const webhookRouter = require('./dashboard/webhook_router');
const appV3_Router = require('./app_services/v3_/route');
/********************
 * 
 * 
 * Api Configuration
 * 
 * 
 */
const api_app = express();
const api_port = config.api_port;
api_app.use(express.static('dist/api_dist')); //api dist
api_app.engine('html', HoganExpress);
// By default, Express will use a generic HTML wrapper (a layout) to render all your pages. If you don't need that, turn it off.
api_app.set('view options', {
    layout: true
});
api_app.set('layout', 'container');
api_app.set('views', __dirname + '/dist/api_dist');
api_app.set('view engine', 'html');
api_app.use(bodyParser.text({ limit: config.BodyParserLimit }))
api_app.use(bodyParser.raw({ limit: config.BodyParserLimit }));
api_app.use(bodyParser.json({ limit: config.BodyParserLimit }));
api_app.use(bodyParser.urlencoded({ extended: true, limit: config.BodyParserLimit, parameterLimit: config.ParameterLimit }));
api_app.use(morgan(function (tokens, req, res) {
    let method = tokens.method(req, res);
    let url = tokens.url(req, res);
    let resptime = `${tokens['response-time'](req, res)} ms`;
    return `${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)}`;
}))
api_app.use(CookieParser());
api_app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

api_app.use(rollbar.errorHandler());

api_app.use('/webhook', webhookRouter);
api_app.use('/xadmin', oldDashboardRouter);
api_app.use('/admin', dashboardRouter);
api_app.use('/upload', uploadRouter);
api_app.use('/app/v1', appV1Router);
api_app.use('/app/v2', appV2Router);
api_app.use('/app/v3', appV3Router);
api_app.use('/app/v2.1', appV2_1Router);
api_app.use('/app/v2.2', appV2_2Router);
api_app.use('/app/v3_', appV3_Router);

createFolders();

// catch 404 and forward to error handler
api_app.use(function (req, res, next) {
    return res.render('index');
});

api_app.listen(api_port, () => {
    scheduleJobs.initJobs();
    console.log(`Anynews Api Server started on ${api_port}`);
});

/********************
 * 
 * 
 * Admin Configuration
 * 
 * 
 */
const admin_app = express();
const admin_port = config.admin_port;
admin_app.use(express.static('dist/admin_dist')); //admin dist
// admin_app.use('/events', express.static('events/current_event'));
admin_app.engine('html', HoganExpress);
// By default, Express will use a generic HTML wrapper (a layout) to render all your pages. If you don't need that, turn it off.
admin_app.set('view options', {
    layout: true
});
admin_app.set('layout', 'container');
admin_app.set('views', __dirname + '/dist/admin_dist');
admin_app.set('view engine', 'html');
admin_app.use(bodyParser.text({ limit: config.BodyParserLimit }))
admin_app.use(bodyParser.raw({ limit: config.BodyParserLimit }));
admin_app.use(bodyParser.json({ limit: config.BodyParserLimit }));
admin_app.use(bodyParser.urlencoded({ extended: true, limit: config.BodyParserLimit }));
admin_app.use(morgan(function (tokens, req, res) {
    let method = tokens.method(req, res);
    let url = tokens.url(req, res);
    let resptime = `${tokens['response-time'](req, res)} ms`;
    return `${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)}`;
}))
admin_app.use(CookieParser());
admin_app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

admin_app.use('/public',express.static('public'));

admin_app.use('/',dashboardRouter);

// catch 404 and forward to error handler
admin_app.use(function (req, res, next) {
    return res.render('index');
});
admin_app.listen(admin_port, () => {
    console.log(`Anynews Admin Server started on ${admin_port}`);
});


const new_admin = express();
const new_admin_port = config.new_admin_port;
new_admin.use(express.static('dist/new_admin_dist')); //admin dist
new_admin.engine('html', HoganExpress);
// By default, Express will use a generic HTML wrapper (a layout) to render all your pages. If you don't need that, turn it off.
new_admin.set('view options', {
    layout: true
});
new_admin.set('layout', 'container');
new_admin.set('views', __dirname + '/dist/new_admin_dist');
new_admin.set('view engine', 'html');
new_admin.use(bodyParser.text({ limit: config.BodyParserLimit }))
new_admin.use(bodyParser.raw({ limit: config.BodyParserLimit }));
new_admin.use(bodyParser.json({ limit: config.BodyParserLimit }));
new_admin.use(bodyParser.urlencoded({ extended: true, limit: config.BodyParserLimit }));
new_admin.use(morgan(function (tokens, req, res) {
    let method = tokens.method(req, res);
    let url = tokens.url(req, res);
    let resptime = `${tokens['response-time'](req, res)} ms`;
    return `${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)}`;
}))
new_admin.use(CookieParser());
new_admin.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

new_admin.use('/public',express.static('public'));

// catch 404 and forward to error handler
new_admin.use(function (req, res, next) {
    return res.render('index');
});
new_admin.listen(new_admin_port, () => {
    console.log(`Anynews New Admin Server started on ${new_admin_port}`);
});

// if( process.env.NODE_ENV == "production" ){
//     require("./helpers/firebase_events").firebaseRef();
// }