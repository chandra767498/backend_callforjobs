let MessagesController = function () { };

const request = require('request');

const MSG91Controller = require("./MSG91Controller");
const CommonController = require("../../../helpers/CommonController");

MessagesController.Send_OTP_TO_Mobile = (PhoneNumber, OTP, MessageID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Message = `<#> Welcome to Any News, Your OTP for login is  ${OTP}. Message ID: ${MessageID}`;
                let Result = await MessagesController.Find_Default_SMS_Provider_and_Send_SMS(PhoneNumber, Message);
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

MessagesController.Find_Default_SMS_Provider_and_Send_SMS = (PhoneNumber, Message) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // let data = {
                //     "to": PhoneNumber,
                //     "content": Message,
                //     "from": "Anynews",
                //     "dlr": "yes",
                //     "dlr-method": "GET",
                //     "dlr-level": 2,
                //     "dlr-url": "https://anynews.co.in"
                // }
                // var options = {
                //   'method': 'POST',
                //   'url': 'https://rest-api.d7networks.com/secure/send',
                //   'headers': {
                //     'Content-Type': 'application/x-www-form-urlencoded',
                //     'Authorization': 'Basic cmR2dTY3NDM6bTBUTXlRa0Y='
                //   },
                //   body: JSON.stringify(data)
                // };
                // request(options, function (error, response) {
                //   // if (error) throw new Error(error);
                //   console.log(JSON.parse(response.body), error);
                //   resolve(true);
                // });
                let ProviderData = await CommonController.Common_Find_Default_SMS_Provider();
                if (ProviderData == null) {
                    let Result = await MSG91Controller.Send_SMS(PhoneNumber, Message);
                    resolve(Result);
                } else {
                    if (ProviderData.Service_Type == 1) {
                        //Msg91
                        let Result = await MSG91Controller.Send_SMS(PhoneNumber, Message);
                        resolve(Result);
                    } else {
                        // In future we have to implement other providers
                        let Result = await MSG91Controller.Send_SMS(PhoneNumber, Message);
                        resolve(Result);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

module.exports = MessagesController;