const mongoose=require('mongoose');

//JObs schema
const jobSchema=new mongoose.Schema({
    jobpostedby:{
        type:String
    },
    jobtitle:{
        type:String,
        required:true
    },
    jobid:{
        type:Number
    },
    requiredexp:{
        type:Number,
        required:true
    },
    location:{
        type:String,
        
    },
    keyskills:{
        type:String,
        required:true
    },
    jobdesc:{
        type:String,
        required:true
    },
    companyprofile:{
        type:String
    }
})
jobSchema.index({ jobpostedby:1})
jobSchema.index({jobtitle:1})
jobSchema.index({ jobid:1})
jobSchema.index({requiredexp:1})
jobSchema.index({ location:1})
jobSchema.index({keyskills:1})
const jobsModel=mongoose.model('jobs',jobSchema);
module.exports=jobsModel;