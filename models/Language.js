const mongoose = require("../config/connect");

const Language = mongoose.Schema({
    LanguageID: { type: String, default: "" },
    Language_Name: { type: String, default: "" },
    Language_Type: {type: String, default: ""},
    Time: {type: Date, default: null },
    SNo: { type: Number, default: 0 },
    id: { type: Number, unique: true },
    name: { type: String, default: "" },
    title: { type: String, default: "" },
    type: { type: String, default: "live" },
    // first_image: { type: String, default: "" },
    // second_image: { type: String, default: "" },
    // is_test: { type: Boolean, default: false },
    status: { type: Boolean, default: true }
}, 
{ 
    timestamps: true,
    collection: "Language" 
});

module.exports = mongoose.callforjobs.model('Language', Language);