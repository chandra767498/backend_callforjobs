const helper = require("../../../helpers/helper");
const DeviceService = require("../controller/DeviceService");
const DeviceController = {};
const EncDecNode = require("../../../utils/EncDecNode");
const CommonController = require("../../../helpers/CommonController");
const devices = require("../../../models/devices");
DeviceController.Splash_Screen = async (req, res) => {
  // console.log(req.body)
  // return;
  try {
    let body = req.body;
    // if( req.body.data ){
    //     body = await EncDecNode.decrypt( req.body.data );
    // }
    let failResponse = {},
      failStatus = 0;
    if (!body.hasOwnProperty("draft_id")) {
      failStatus = 1;
      failResponse = { success: false, msg: "Draft id is required." };
    }
    if (!body.AppVersion) {
      failStatus = 1;
      failResponse = { success: false, msg: "App version is required." };
    }
    if (body.draft_id == 0) {
      if (!body.DeviceID) {
        failStatus = 1;
        failResponse = { success: false, msg: "Device id is required." };
      }
      if (!body.DeviceType || ![1, 2].includes(body.DeviceType)) {
        failStatus = 1;
        failResponse = { success: false, msg: "Device type is required." };
      }
      if (!body.DeviceName) {
        failStatus = 1;
        failResponse = { success: false, msg: "Device name is required." };
      }
      if (!body.fcm_token) {
        failStatus = 1;
        failResponse = { success: false, msg: "Fcm token is required." };
      }

      if (failStatus) {
        console.log("------------------------------------------", failResponse);
        // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(failResponse)) });
        return res.send(failResponse);
      }
      let IPAddress = await helper.Common_IP_Address(req);
      let Response = await DeviceService.Get_Splash_Screen_Info(
        body,
        IPAddress
      );
      console.log("------------------------------------------", Response);
      
      return res.send(Response);
    }
  } catch (error) {
    return res.send({ data: errorResponse });
  }
};

DeviceController.Splash_Screen_Animation = (values) => {
  return new Promise((resolve, reject) => {
    setImmediate(async () => {
      try {
        let query = {
          DeviceID: values.DeviceID,
        };
        await devices.findOne(query);
        resolve({
          success: true,
          extras: {
            Status: "Device Splash Screen Completed Successfully",
            animation:
              "https://newsany.s3-us-west-2.amazonaws.com/Test-Animation-1.gif",
          },
        });
      } catch (error) {
        reject(await CommonController.Common_Error_Handler(error));
      }
    });
  });
};
DeviceController.Disable_Update_Menu = async (req, res) => {
  try {
    // if( !req.body.data ){
    //     return res.send({ data: await EncDecNode.encrypt(JSON.stringify({ success: false, msg: "Data is required." })) });
    // }

    // let body = await EncDecNode.decrypt( req.body.data );
    let body = req.body;

    let failResponse = {},
      failStatus = 0;
    if (!body.hasOwnProperty("draft_id")) {
      failStatus = 1;
      failResponse = { success: false, msg: "Draft id is required." };
    }
    if (failStatus) {
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(failResponse)) });
      return res.send(failResponse);
    }
    let Response = await DeviceService.Disable_Update_Menu(body);
    return res.send(Response);
    // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(Response)) });
  } catch (error) {
    if (!res.headersSent) {
      let errorResponse = await CommonController.Common_Error_Handler(error);
      return res.send(errorResponse);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(errorResponse)) });
    }
  }
};

module.exports = DeviceController;
