const mongoose = require("../config/connect");

const Job_category = mongoose.Schema({
    category_id: { type: Number, default: 0 },
    // Category_Name: { type: String, default: "" },
    // Category_Type: {type: String, default: ""},
    // Time: {type: Date, default: null },
    SNo: { type: Number, default: 0 },
    id: { type: Number, unique: true },
    name: { type: String, default: "" },
    // title: { type: String, default: "" },
    type: { type: String, default: "live" },
    // first_image: { type: String, default: "" },
    // second_image: { type: String, default: "" },
    // is_test: { type: Boolean, default: false },
    status: { type: Boolean, default: true }
}, 
{ 
    timestamps: true,
    collection: "Job_category" 
});

module.exports = mongoose.callforjobs.model('Job_category', Job_category);