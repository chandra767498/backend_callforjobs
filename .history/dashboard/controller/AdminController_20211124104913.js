let AdminController = function() {};
const tzmoment = require('moment-timezone');
const crypto = require("crypto");
const uuid = require("uuid");
const CommonController = require("../../helpers/CommonController");
const Language = require("../../models/Language");
const Admins = require("../../models/Admins");
const Logout_Events = require("../../models/logout_events");
const Employee_Login_Stats = require('../../models/Employee_Login_Stats');
const Job_category = require('../../models/Job_category');
const Static_Data = require('../../models/Static_Data');
const companies = require('../../models/companies');
const Job_Posting = require('../../models/Job_Posting');
const States = require('../../models/States');
const Cities = require('../../models/Cities');


//Language
AdminController.Add_Language = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let Data = {
                    id: await CommonController.generateSequenceNumber("language"),
                    title: values.title.trim(),
                    name: values.name.trim(),
                    type: values.type
                };
                let SaveResult = await Language(Data).save();
                resolve({ success: true, msg: "Language Added Successfully" });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Languages = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let sortOptions = {
                    id: 1
                };
                let viewOptions = {
                    _id: 0,
                    id: 1,
                    title: 1,
                    name: 1,
                    type: 1
                }
                let Result = await Language.find({}, viewOptions).sort(sortOptions).lean().exec();
                resolve({ success: true, data: Result });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Language = (values) => {
  return new Promise(async (resolve, reject) => {
    try {
      let query = {
        id: values.language_id,
      };
      let changes = {
        $set: {
          title: values.title.trim(),
          name: values.name.trim(),
          type: values.type,
        },
      };
      let updateOptions = {
        upsert: true,
        setDefaultsOnInsert: true,
        new: true,
      };
      let Result = await Language.findOneAndUpdate(
        query,
        changes,
        updateOptions
      ).lean();
      resolve({ success: true, msg: "Updated Successfully" });
    } catch (error) {
      reject(await CommonController.Common_Error_Handler(error));
    }
  });
};
AdminController.Add_Static_Language = (values) => {
    return new Promise(async(resolve, reject) => {

        try {
            let query = {
                language_id: values.language_id
            }
            let Result = await Static_Data.findOne(query).lean().exec();
            if (Result == null) {
                let Data = {
                    language_id: values.language_id,
                    language_wise_data: [{
                        static_id: 1,
                        title: values.title.trim().toLowerCase(),
                        name: values.name.trim(),
                        status: true
                    }]
                }
                let SaveData = await Static_Data(Data).save();
                resolve({ success: true, msg: "Static Language Added Successfully" })
            } else {
                let value = Result.language_wise_data.filter((data) => {
                    return data.title == values.title.trim().toLowerCase()
                })
                if (value.length) {
                    reject({ sucess: false, msg: "Static Language already exists." });
                } else {
                    let data = {
                        static_id: Result.language_wise_data.length + 1,
                        title: values.title.trim().toLowerCase(),
                        name: values.name.trim(),
                        status: true
                    }
                    await Static_Data.updateOne({ language_id: values.language_id }, { $push: { language_wise_data: data } });
                    resolve({ sucess: true, msg: "Added Successfully" });
                }
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}
// AdminController.Admin_Fetch_Static_Data = (values, AdminData) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async() => {
//             try {
//                 let query = {
//                     language_id: values.language_id,
//                 }
//                 let Count = await Static_Data.countDocuments(query);
//                 let Result = await Static_Data.findOne(query).lean();
//                 if (Result) {
//                     resolve({ success: true, count: Count, data: Result.language_wise_data });
//                 } else {
//                     resolve({ success: false, msg: "No Data" });
//                 }
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }


//Jobs
AdminController.Add_job_category = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let Data = {
                    id: await CommonController.generateSequenceNumber("language"),
                    // title: values.title,
                    name: values.name.trim(),
                    type: values.type
                };
                let SaveResult = await Job_category(Data).save();
                resolve({ success: true, msg: "Job Category Added Successfully" });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_company = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let Data = {
                    // id: await CommonController.generateSequenceNumber("language"),
                    name: values.name.trim(),
                    category: values.category,
                    mobile: values.mobile,
                    url: values.url
                };
                let SaveResult = await companies(Data).save();
                resolve({ success: true, msg: "Company Added Successfully" });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_Admin_Email_Registered = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let query = {
                    $or: [
                        { EmailID: values.email_id },
                        { email_id: values.email_id }
                    ]
                };
                let Result = await Admins.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: "EMAIL NOT REGISTERED" } })
                }
                else if( !Result.status ){
                    reject({ success: false, extras: { msg: "Inactive Account." } })
                }
                else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//Login
AdminController.Login = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let Password = String(values.pwd);
                let PasswordSalt = AdminData.pwd_salt;
                let pass = Password + PasswordSalt;
                let PasswordHash = crypto.createHash('sha512').update(pass).digest("hex");
                if (AdminData.PasswordHash == PasswordHash || AdminData.pwd_hash == PasswordHash) {
                    let fndupdquery = {
                        id: AdminData.id
                    };
                    let fndupdchanges = {
                        $set: {
                            session_id: uuid.v4()
                                // SessionID: uuid.v4(),
                                // updated_at: new Date()
                        }
                    };
                    let fndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let Result = await Admins.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v -PasswordHash -PasswordSalt -Status -updated_at').lean();
                   
                    
                    // if (AdminData.Whether_Employee == undefined) {
                    //     AdminData.Whether_Employee = false
                    // }
                    // if (AdminData.Employee_Permissions == undefined) {
                    //     AdminData.Employee_Permissions = {}
                    // }
                    let dayStartTime = parseInt(tzmoment(new Date()).tz('asia/kolkata').startOf('day').utc().format('x'));
                    let dayEndTime = parseInt(tzmoment(new Date()).tz('asia/kolkata').endOf('day').utc().format('x'));
                    
                    let logouts = await Logout_Events.countDocuments({ employee_id: AdminData.id, logout_type: 1, createdAt: { $gte: dayStartTime, $lt: dayEndTime } });
                    resolve({ success: true, msg: "Login Successfully", data: Result, logouts: logouts});

                    let date = tzmoment(new Date()).tz('asia/kolkata').startOf('day').utc().format('x');
                    let matchQuery = {
                        employee_id: AdminData.id,
                        work_date: date
                    }
                    let updateQuery = {
                        $setOnInsert: {
                            first_post_id: 0,
                            last_post_id: 0,
                            first_post_time: 0,
                            last_post_time: 0,
                            login_at: new Date().getTime()
                        },
                        $set: {
                            logout_at: new Date().getTime()
                        }
                    }
                    let employeeLoginStats = await Employee_Login_Stats.findOneAndUpdate(matchQuery, updateQuery, fndupdoptions);
                    console.log(employeeLoginStats);
                } else {
                    reject({ success: false, extras: { msg: "INVALID PASSWORD" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



AdminController.Logout = async(values) => {
    let data = {
        employee_id: values.admin_id,
        logout_type: values.logout_type,
        last_event: values.last_event
    }
    let SaveResult = await new Logout_Events(data).save();
    if (SaveResult) {
        return { success: true, msg: "Logout Successful" };
    }
    return { success: false, msg: "Logout Unsuccessful" };
}


AdminController.Check_Whether_Admin_Email_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                let query = {
                    // EmailID: values.EmailID,
                    // Status: true
                    email_id: values.email_id,
                    status: true
                };
                let Result = await Admins.findOne(query).lean();
                if (Result == null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: "EMAIL ALREADY REGISTERED" } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.Create_Admin_User = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async() => {
            try {
                // let Password = String(values.Password);
                let Password = String(values.pwd);
                let PasswordSalt = await CommonController.Random_OTP_Number();
                let pass = Password + PasswordSalt;
                let state_id = 0,
                    dist_id = 0;
                switch (values.language_id) {
                    case 1:
                    case 2:
                        state_id = 35;
                        dist_id = 228;
                    case 3:
                        state_id = 33;
                        dist_id = 16;
                        break;
                    case 4:
                        state_id = 32;
                        dist_id = 116;
                        break;
                    case 5:
                        state_id = 16;
                        dist_id = 71;
                        break;
                    case 6:
                        state_id = 26;
                        dist_id = 51;
                        break;
                }
                let Data = {
                    id: await CommonController.generateSequenceNumber("employee_id"),
                    name: values.name,
                    email_id: values.email_id,
                    pwd_hash: crypto.createHash('sha512').update(pass).digest("hex"),
                    pwd_salt: PasswordSalt,
                    is_admin: values.is_admin,
                    permissions: values.is_admin ? {} : values.permissions,
                    // language_id: values.language_id,
                    // role: values.role || "Reporter",
                    // state_id: state_id,
                    // dist_id: dist_id,
                    profile_pic: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp",
                    original_profile_pic: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp"
                };
                let SaveResult = await Admins(Data).save();

                // let writerObj = {
                //     writer_id: Data.id,
                //     writer_type: "employee",
                //     writer_name: Data.name,
                //     profile_pic: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp",
                //     role: Data.role,
                //     status: true,
                //     posts: 0,
                //     likes: 0,
                //     followers: 0,
                //     state_id: Data.state_id,
                //     dist_id: Data.dist_id,
                //     reporter_status: 1,
                //     language_id: Data.language_id,
                //     createdAt: new Date(),
                //     updatedAt: new Date()
                // }
                // await new Writers(writerObj).save();

                resolve({ success: true, extras: { Status: "Admin Created Successfully" }, msg: "Admin Created Successfully" });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//Employees
AdminController.List_All_Employees = (values, AdminData) => {
    return new Promise((resolve, reject) => {
      setImmediate(async () => {
        try {
          let query = {
            status: true,
            is_admin: false,
          };
          let toSkip = parseInt(values.skip);
          let toLimit = parseInt(values.limit);
          let sortOptions = {
            name: 1,
          };
          let Count = await Admins.countDocuments(query).lean().exec();
          let Result = await Admins.find(query)
            .select("-_id")
            .sort(sortOptions)
            .lean()
            .skip(toSkip)
            .limit(toLimit)
            .exec();
          resolve({ success: true, count: Count, data: Result });
        } catch (error) {
          reject(await CommonController.Common_Error_Handler(error));
        }
      });
    });
  };

AdminController.Edit_Employee = (values, AdminData) => {
    return new Promise((resolve, reject) => {
      setImmediate(async () => {
        try {
          let query = {
            id: values.employee_id,
          };
          let changes = {
            $set: {
              name: values.name.trim(),
              email_id: values.email_id.trim(),
              permissions: values.permissions,
            //   language_id: values.language_id,
            },
          };
          let updatedStatus = await Admins.findOneAndUpdate(query, changes, {
            returnNewDocument: true,
          });
          resolve({ sucess: true, msg: "Updated Sucessfully" });
        } catch (error) {
          reject(await CommonController.Common_Error_Handler(error));
        }
      });
    });
  };
  
  
AdminController.Active_Inactive_Employee = (values, AdminData) => {
    return new Promise((resolve, reject) => {
      setImmediate(async () => {
        try {
          let query = {
            id: values.employee_id,
            is_admin: false,
          };
          let Result = await Admins.findOne(query).lean();
          if (Result != null) {
            let status = Result.status ? false : true;
            let changes = {
              $set: {
                status: status,
              },
            };
            let updatedStatus = await Admins.findOneAndUpdate(
              query,
              changes
            ).lean();
            resolve({
              success: true,
              msg: "Employee Status Updated Successfully",
            });
  
            await Writers.findOneAndUpdate(
              { writer_id: values.employee_id, writer_type: "emmployee" },
              { $set: { status: status } }
            );
          } else {
            resolve({ success: false, msg: "Admin can't be inactive" });
          }
        } catch (error) {
          reject(await CommonController.Common_Error_Handler(error));
        }
      });
    });
  };

//Posting
  AdminController.Add_Job_Posting = async (values) => {
    // let employee = await Job_Posting.findOne({ emp_id: values.emp_id }).lean();
    // if (employee) {
    //   return { success: false, msg: "Employee id already exist." };
    // }
    values.admin_id = null;
    values.session_id = null;
    values.id = await CommonController.generateSequenceNumber("employee_id");
    values.status = true;
    let empObj = {
        // id: await CommonController.generateSequenceNumber("employee_id"),
        // emp_id: values.emp_id,
        company: values.company,
        title: values.title,
        description: values.description,
        email: values.email,
        job_contact: values.job_contact,
        job_skills: values.job_skills,
        country_id: values.country_id,
        state_id: values.state_id,
        city_id: values.city_id,
        salaryfrom: values.salaryfrom,
        salaryto: values.salaryto,
        peroid: values.peroid,
        categories: values.categories,
        type: values.type,
        position: values.position,
        gender: values.gender,
        date: values.date,
        education: values.education,
        experience: values.experience,
        status: values.status
    }
    await new Job_Posting(values).save();
    return { success: true, msg: "Added Successfully." };
  };
  
  AdminController.view_states = (values) => {
    return new Promise(async (resolve, reject) => {
      try {
        let matchQuery = {
          status: true,
        };
        // if (values.language_id) {
        //   matchQuery["language_id"] = values.language_id;
        // }
        let viewOptions = {
          _id: 0,
          id: 1,
          title: 1,
          name: 1,
          status: 1,
        //   language_id: 1,
        };
        let Result = await States.find(matchQuery, viewOptions)
          .sort({ title: 1 })
          .lean()
          .exec();
        resolve({ success: true, data: Result });
      } catch (error) {
        reject(await CommonController.Common_Error_Handler(error));
      }
    });
  };

  AdminController.Get_Job_Posting_Details = async (values) => {
    let viewOptions = {
      // _id: 0,
      id: 1,
      company: 1,
      title: 1,
      city_id: 1,
      description: 1,
      status: 1,
    };
    let query = {
      status: true,
    };
    if (values.hasOwnProperty("status")) {
      query["status"] = values.status;
    }
    let jobsData = await Job_Posting.find(query, viewOptions)
      .sort({ id: 1 })
      .lean();
    return { success: true, data: jobsData };
  };
  AdminController.view_cities = (values) => {
    return new Promise(async (resolve, reject) => {
      try {
        let query = {
          status: true,
        };
  
        if (values.state_id) {
          query["state_id"] = values.state_id;
        // } else if (values.language_id) {
        //   query["language_id"] = values.language_id;
        } else {
          //nothing
        }
  
        let sortOptions = {
          title: 1,
        };
        let Count = await Cities.countDocuments(query).lean().exec();
        let Result = await Cities.find(query)
          .select("-_id")
          .sort(sortOptions)
          .lean()
          .exec();
        resolve({ success: true, count: Count, data: Result });
      } catch (error) {
        reject(await CommonController.Common_Error_Handler(error));
      }
    });
  };
  

  AdminController.List_All_job_Category = (values, AdminData) => {
    return new Promise((resolve, reject) => {
      setImmediate(async () => {
        try {
          let query = {
            status: true,
            // is_admin: false,
          };
          let toSkip = parseInt(values.skip);
          let toLimit = parseInt(values.limit);
          let sortOptions = {
            name: 1,
          };
          let Count = await Job_category.countDocuments(query).lean().exec();
          let Result = await Job_category.find(query)
            // .select("-_id")
            .sort(sortOptions)
            .lean()
            .skip(toSkip)
            .limit(toLimit)
            .exec();
          resolve({ success: true, count: Count, data: Result });
        } catch (error) {
          reject(await CommonController.Common_Error_Handler(error));
        }
      });
    });
  };
module.exports = AdminController;

