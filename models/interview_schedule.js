const mongoose = require("../config/connect");
const { Schema } = require("mongoose");

const interview_schedule = mongoose.Schema({
    id: { type: Number, unique: true },
    name: { type: String, default: '' },
    company: { type: String, default: '' },
    designation: { type: String, default: '' },
    mobile: { type: Number, default: 0 },
    email_id:{ type: String, default: '' },
    status:{ type: String, default: '' },
    date: { type: Date, default: null },
    description: { type: String, default: '' },
    employee: { type: String, default: '' },


   
},
{
    timestamps: true,
    collection: 'interview_schedule'
});

module.exports = mongoose.callforjobs.model('interview_schedule', interview_schedule);