const mongoose = require("../config/connect");
const { Schema } = require("mongoose");

const employee_status = mongoose.Schema({
    id: { type: Number, unique: true },
    name: { type: String, default: '' },
    company: { type: String, default: '' },
    designation: { type: String, default: '' },
    calls: { type: Number, default: 0 },
    rejected:{ type: Number, default: 0 },
    follows:{ type: Number, default: 0 },
    date: { type: Date, default: null },
   
},
{
    timestamps: true,
    collection: 'employee_status'
});

module.exports = mongoose.callforjobs.model('employee_status', employee_status);