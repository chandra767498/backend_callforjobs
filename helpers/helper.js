
const MSG91 = require("msg91");
const Counters=require('../models/counters')
const config = require("../config/config");
const Language = require("../models/Language");

const App_Versions_Settings = require("../models/App_Versions_Settings");
let msg91 = MSG91(config.msg91.authkey, config.msg91.sender_id, config.msg91.route_no);

module.exports = {

    Random_OTP_Number: () => {
        let charBank = "123456789";
        let str = '';
        for (let i = 0; i < 4; i++) {
            str += charBank[parseInt(Math.random() * charBank.length)];
        };
        return str;
    },

    Send_SMS: async (PhoneNumber, Message) => {
        try {
            let MessageSent = await msg91.send(PhoneNumber, Message);
            return true;
        } catch (error) {
            console.error("MSG91 error----->", error);
            return false;
        }
    },
    generateSequenceNumber:async(collection_name) => {
                try {
                    const data = await Counters.findOneAndUpdate(
                        { collection_name: collection_name }, 
                        { $inc: { seq_no: 1 } },
                        { new: true, upsert: true });
                        return (data.seq_no);
                } catch (error) {
                    return { success:false,msg:"Internal error"}
                }
            
        
    },
    Common_IP_Address:async(req) => {
                try {
                    let IPAddress = req.headers["x-forwarded-for"];
                    if (IPAddress) {
                        let list = IPAddress.split(",");
                        IPAddress = list[list.length - 1];
                    }else {
                        IPAddress = req.connection.remoteAddress;
                    }
                    IPAddress = (IPAddress == '::1') ? '0:0:0:0' : IPAddress;
                    return (IPAddress);
                } 
                catch (error) {
                    return { success:false,msg:"Internal error"}
                }
      },

      Common_Register_or_Get_App_versions :async() => {
        
                try {
                    let Result = await App_Versions_Settings.findOne().lean();
                    if (Result == null) {
                        let VersionData = await App_Versions_Settings().save();
                        return (JSON.parse(JSON.stringify(VersionData)));
                    } else {
                        return (Result);
                    };
                } catch (error) {
                    return { success:false,msg:"Internal error"}
                }
           
        }

}