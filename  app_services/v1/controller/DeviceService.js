const helper = require("../../../helpers/helper");
const mongoose = require("../../../config/connect");
const tzmoment = require('moment-timezone');
const DeviceDraft=require('../../../models/devices');
const Root_Settings = require("../../../models/rootsettings");
const Installed_Devices = require("../../../models/Installed_Devices");
const UserActivity = require("../../../models/userActivity");
const DeviceService={}
DeviceService.Get_Splash_Screen_Info = async (values, ip) => {
    // console.log(values);
    let draft_id = values.draft_id, device_id = values.DeviceID, 
        device_name = values.DeviceName, device_type = values.DeviceType, 
        fcm_token = values.fcm_token || "", app_version = values.AppVersion,
        language_id = 0;
    
    let draftQuery = { "id": draft_id };
    if( !draft_id ){
        draftQuery = { "device_id": device_id };
    }
    let is_new_device = true, mobile_number = "", update_menu_status = 1, refer_status = false;
    let draftData = await DeviceDraft.findOne( draftQuery ).lean();
    if( draftData ){
        draft_id = draftData.id;
        device_id = draftData.device_id;
        device_name = draftData.device_name;
        device_type = draftData.device_type;
        language_id = draftData.language_id;
        is_new_device = false;
        mobile_number = draftData.mobile_number;
        update_menu_status = draftData.update_menu_status;
        refer_status = draftData.refer_status;
    }

    let versionData = await helper.Common_Register_or_Get_App_versions();
    let latestVersion = ( device_type == 1 ) ? versionData.Android_Version : versionData.IOS_Version;
    let app_update = ( app_version >= latestVersion ) ? true : false;
    if( update_menu_status != 0 ){
        if( update_menu_status == 1 && (app_version < latestVersion) ){
            update_menu_status = 2;  // 2 - normal update, 3 - force update
        }
        else if( app_version == latestVersion ){
            update_menu_status = 1;
        }
    }

    let matchQuery = { device_id: device_id };
    let changes = {
        "$set": {
            app_version: app_version,
            update_menu_status: update_menu_status,
            active_status: true
        }
    };

    if(fcm_token){
        changes["$set"]["fcm_token"] = fcm_token;
    }

    if( !values.draft_id ){     //values.draft_id required instead of draft_id
        changes = {
            $setOnInsert: {
                id: draftData ? draftData.id : await helper.generateSequenceNumber("device_drafts"),
                device_id: device_id,
                device_name: device_name,
                device_type: device_type,
                language_id: language_id,
                static_data_api: 0,
                refer_status: refer_status,
                install_source: "google"
            },
            $set: {
                
                ip_address: ip,
                app_version: app_version,
                fcm_token: fcm_token,
                ad_id: values.ad_id || "",
                user_id: 0,
                user_name: "Guest",
                email_id: "",
                mobile_number: "",
                profile_pic: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp",
                original_profile_pic: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp",
                status: true,
                install_time: new Date(),
                notification_flag: true,
                update_menu_status: update_menu_status,
                reinstall_time: draftData ? new Date() : null
            },
            $inc: {
                reinstalls: draftData ? 1 : 0
            }
        };
        mobile_number = "";
    }
    let updateOptions = {
        upsert: true,
        setDefaultsOnInsert: true,
        new: true
    }

    const updatedData = await DeviceDraft.findOneAndUpdate(matchQuery, changes, updateOptions).lean();
    draft_id = updatedData.id;
    
    let font_size = ( language_id == 4 ) ? 13 : 11;     //default font sizes
    let show_animation = 0;
    
    let next_screen = "main";        // Language Screen, States Screen, Districts Screen, Main screen
   
     if( !language_id ){
        next_screen = "language";
    }
   
    values.device_id = device_id;
    splashScreenActivities(values, draft_id, ip, is_new_device, refer_status);    //device activities
    let rootSettings = await Root_Settings.findOne().lean();
    let event_status = false;
    if(rootSettings){
        event_status = rootSettings.event_status;
    }
    let data = {
        draft_id: draft_id,
        font_size: font_size,
        next_screen: next_screen,
        show_animation: show_animation,
        // animation_url: animation_url,
        app_update: app_update,
        language_id: language_id,
        static_data_api: language_id ? values.draft_id ? updatedData.static_data_api : 1 : 0,
        is_login: mobile_number ? true : false,
        update_menu_status: update_menu_status,
        event_status: event_status
    }
    // console.log('data>>>>>>>>', data);
    return { success: true, data: data };
}

async function splashScreenActivities( values, draft_id, ip, is_new_device, refer_status ){
    try{
        if ( !values.draft_id ) {
            let installData = {
                DeviceID: values.DeviceID,
                DeviceType: values.DeviceType,
                DeviceName: values.DeviceName,
                AppVersion: values.AppVersion,
                IPAddress: ip,
                InstallTime: new Date(),
                created_at: new Date(),
                updated_at: new Date()
            }
            let InstalledDeviceData = await new Installed_Devices(installData).save();  //daily installs
        }

        if( ( is_new_device || !refer_status ) && values.referred_by ){
        	let user = await Users.findOne({ ref_long_link: values.referral_link }).lean();
        	if( user && user.id == values.referred_by ){
                let referData = await Referrals.findOne({ referred_to: draft_id }).lean();
                if( !referData ){
                    let newReferralData = {
                        id: await CommonController.generateSequenceNumber("referral"),
                        referred_by: values.referred_by,
                        referred_to: draft_id,
                        referral_link: values.referral_link,
                        install_time: new Date()
                    }
                    await Referrals( newReferralData ).save();
                    await DeviceDraft.updateOne({ id: draft_id }, { $set: { refer_status: true } });

                    let permissions = await Root_Settings.findOne().lean();
                    if( permissions && permissions.referral_status ){
                        let min = 3, max = 6;
                        let userReferrals = await Referrals.countDocuments( { referred_by: values.referred_by } );
                        let cardId = await CommonController.generateSequenceNumber("scratch_card");
                        // if( cardId % 20 == 0 ){
                        // 	min = 10;
                        // 	max = 15;
                        // }
                        let points = 3;
                        switch( userReferrals % 20 ){
                            case 2:
                            case 4:
                            case 8:
                            case 11:
                            case 13:
                            case 16:
                                points = 3;
                                break;
                            case 3:
                            case 6:
                            case 10:
                            case 14:
                            case 17:
                            case 19:
                                points = 4;
                                break;
                            case 1:
                            case 7:
                            case 9:
                            case 15:
                            case 18:
                                points = 5;
                                break;
                            case 5:
                            case 12:
                                points = 6;
                                break;
                            case 0:
                                let min = 10, max = 15;
                                points = Math.floor(Math.random() * (max - min) + min);
                                break;
                        }
                        
                        // if( userReferrals % 20 == 0 ){
                        //     min = 10, max = 15;
                        //     points = Math.floor(Math.random() * (max - min) + min);
                        // }
                        let instantScratchCard = {
                            id: cardId,
                            referred_by: values.referred_by,
                            referred_to: draft_id,
                            card_type: 0,
                            points: points, //Math.floor(Math.random() * (max - min) + min),
                            active_time: tzmoment(new Date()).tz('asia/kolkata').startOf('day').utc().format(),
                            scratch_status: false,
                            status: true
                        }
                        await Scrach_Cards( instantScratchCard ).save();

                        let activityScratchCard = {
                            id: await CommonController.generateSequenceNumber("scratch_card"),
                            referred_by: values.referred_by,
                            referred_to: draft_id,
                            card_type: 1,
                            points: 0,
                            active_time: tzmoment(new Date()).tz('asia/kolkata').add(15, 'days').startOf('day').utc().format(),
                            expire_time: tzmoment(new Date()).tz('asia/kolkata').add(45, 'days').startOf('day').utc().format(),
                            scratch_status: false,
                            status: false
                        }
                        await Scrach_Cards( activityScratchCard ).save();
                    }
                }
	        }
        }

        let updateOptions = {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        }

        let dayStartTime = tzmoment().tz("Asia/Kolkata").startOf("day").format();
        let dayEndTime = tzmoment().tz("Asia/Kolkata").endOf("day").format();
        let activityQuery = {
            draft_id: draft_id,
            created_at: {
                $gte: new Date(dayStartTime),
                $lte: new Date(dayEndTime)
            }
        }
        let activityObj = {
            $setOnInsert: {
                device_id: values.device_id,
                draft_id: draft_id,
                created_at: new Date()
            },
            $set: {
                login_at: new Date().getTime(),
                logout_at: new Date().getTime(),
                updated_at: new Date()
            }
        }
        await UserActivity.findOneAndUpdate(activityQuery, activityObj, updateOptions);

        let date = tzmoment().tz("Asia/Kolkata").format("DD_MM_YYYY");
        let collection_name = "user_activity_"+date;
        let matchQuery = {
            draft_id: draft_id
        }

        let userActivity = await mongoose.callforjobs.collection(collection_name).findOne(matchQuery);
        if(userActivity){
            let updateObject = {};
            updateObject['$set'] = {
                logout_at: new Date().getTime(),
                updated_at: new Date()
            }
            let updatedResult = await mongoose.callforjobs.collection(collection_name).findOneAndUpdate(matchQuery, updateObject, updateOptions);  //every day user activity
        }
        else{
            let insertObject = {
                device_id: values.device_id,
                draft_id: draft_id,
                login_at: new Date().getTime(),
                logout_at: new Date().getTime(),
                morning_status: false,
                afternoon_status: false,
                evening_status: false,
                night_status: false,
                created_at: new Date(),
                updated_at: new Date()
            }
            let updatedResult = await mongoose.callforjobs.collection(collection_name).insertOne(insertObject);
        }
    }
    catch(err){
        console.log(err);
    }
    return;
}

DeviceService.Disable_Update_Menu = async (values) => {
    let deviceDraft = await DeviceDraft.findOneAndUpdate({ id: values.draft_id }, { $set: { update_menu_status: 0 } });
    return { success: true, msg: "Successfully Disabled." };
}
module.exports=DeviceService;