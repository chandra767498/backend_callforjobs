const mongoose = require("../config/connect");

const Installed_Devices = mongoose.Schema({
    DeviceID: { type: String, default: "" },
    DeviceType: { type: Number, default: 1 },//1. Android 2.IOS 3. Web
    DeviceName: { type: String, default: "" },
    AppVersion: { type: Number, default: 0 },
    IPAddress: { type: String, default: "" },
    InstallTime: { type: Date, default: new Date() },
    Interval: { type: Number, default: 0 },
    created_at: { type: Date, default: new Date() },
    updated_at: { type: Date, default: new Date() }
}, { collection: "Installed_Devices" });

module.exports = mongoose.callforjobs.model('Installed_Devices', Installed_Devices);