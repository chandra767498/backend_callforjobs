const mongoose = require("mongoose");

const RootSettings = mongoose.Schema({
    referral_status: { type: Boolean, default: true },
    event_login_status: { type: Boolean, default: false },
    event_status: { type: Boolean, default: false }
}, 
{ 
    timestamps: true,
    collection: "root_settings" 
});

module.exports = mongoose.model('root_settings', RootSettings);