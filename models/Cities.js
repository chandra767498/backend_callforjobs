const mongoose = require("../config/connect");

const Cities = mongoose.Schema({
    id: { type: Number },
    name: { type: String },
    title: { type: String },
    state_id: { type: Number },
    city_number: { type: Number, default: 0 },
    status: { type: Boolean, default: true },
},
{
    timestamps: true,
    collection: 'cities'
});
module.exports = mongoose.callforjobs.model('cities', Cities);