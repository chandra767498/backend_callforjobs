let CommonController = function () { };

const Language = require("../models/Language");
const Admins = require('../models/Admins');
const users = require("../models/users");
const App_SMS_Providers = require("../models/App_SMS_Providers")
const Counters = require("../models/counters")

CommonController.Check_Whether_Employee_Email_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    EmailID: values.EmailID,
                    Status: true
                };
                let Result = await users.findOne(query).lean();
                if (Result == null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: "EMAIL ALREADY REGISTERED" } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
CommonController.Check_for_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    // id: values.AdminID
                    id: values.admin_id || values.AdminID
                };
                let Result = await Admins.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, msg: "INVALID ADMIN" })
                } else {
                    // if (Result.SessionID == values.SessionID) {
                    if (Result.session_id == values.session_id || Result.SessionID == values.SessionID) {
                        resolve(Result);
                    } else {
                        reject({ success: false, msg: "SESSION EXPIRED" })
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
CommonController.Check_for_Language = (LanguageID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    LanguageID: LanguageID
                };
                let Result = await Language.findOne(query).select('-_id -__v').lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: "INVALID LANGUAGE" } });
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
CommonController.Common_Error_Handler = (error) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {

            try {
                console.error("Common_Error_Handler------->", error);
                if (error.sucess == null || error.sucess == undefined) {
                    if (error instanceof SyntaxError) {
                        resolve({ sucess: false, msg: "Server Error", extras: { msg: "Server Error" } });
                    } else {
                        resolve({ success: false, msg: "Database Error", extras: { msg: "Server Error" } });
                    }
                } else {
                    resolve(error);
                }
            } catch (error) {
                console.error('Something Error Handler--->', error);
            }
        });
    });
};

CommonController.Random_OTP_Number = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let charBank = "123456789";
                let str = '';
                for (let i = 0; i < 4; i++) {
                    str += charBank[parseInt(Math.random() * charBank.length)];
                };
                resolve(str);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.generateSequenceNumber = (collection_name) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                const data = await Counters.findOneAndUpdate(
                    { collection_name: collection_name }, 
                    { $inc: { seq_no: 1 } },
                    { new: true, upsert: true });

                resolve(data.seq_no);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//login 
CommonController.Common_Email_Validation = EmailID => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (EmailID == "") {
                    resolve("Validated Successfully");
                } else {
                    if (validator.isEmail(EmailID)) {
                        resolve("Validated Successfully");
                    } else {
                        reject({ success: false, extras: { msg: "INVALID EMAIL FORMAT" } });
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Find_Default_SMS_Provider = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Selected_Provider: true,
                    Status: true
                };
                let Result = await App_SMS_Providers.findOne(query).lean();
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

module.exports = CommonController;
