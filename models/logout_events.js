const mongoose = require("../config/connect");
const Schema = mongoose.Schema;

const logout_events = mongoose.Schema({
    employee_id : { type: Number, default: 0 },
    logout_type: { type: Number, default: 0 },
    last_event: {type: String, default: ""},
}, 
{
    timestamps: true,
    collection: 'logout_events'
});

module.exports = mongoose.callforjobs.model('logout_events', logout_events);