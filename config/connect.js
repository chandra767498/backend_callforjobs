const config = require("./config");
let Mongoose = require("mongoose");
Mongoose.Promise = global.Promise;

let MongoURL = config.MongoURL;
let options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}
console.log('mongooo', MongoURL)

// const connectToDb = async () => {
//     try {
//         await Mongoose.connect(MongoURL, options);
//         console.log(`Connected to mongo db--->${MongoURL}`);
//     }
//     catch (error) {
//         console.error('Could not connect to MongoDB');
//     }
// }

Mongoose.callforjobs = Mongoose.createConnection(MongoURL, options);
console.log("connection established with", MongoURL);

module.exports = Mongoose;