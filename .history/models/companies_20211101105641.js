const mongoose = require("../config/connect");

const companies = mongoose.Schema({
    CategoryID: { type: String, default: "" },
    Category_Name: { type: String, default: "" },
    Category_Type: {type: String, default: ""},
    Time: {type: Date, default: null },
    SNo: { type: Number, default: 0 },
    id: { type: Number, unique: true },
    name: { type: String, default: "" },
    category: { type: String, default: "" },
    mobile: { type: String, default: "live" },
    websitelink: { type: String, default: "" }
}, 
{ 
    timestamps: true,
    collection: "companies" 
});

module.exports = mongoose.callforjobs.model('companies', companies);