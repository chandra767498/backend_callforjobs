const mongoose = require("../config/connect");

const users = mongoose.Schema({
    first_name: { type: String, default: '' },
    last_name: { type: String, default: '' },
    email_id: { type: String, default: '' },
    mobile_number: { type: Number, default: 0 },
    dob: { type: Number, default: 0 },
    gender: { type: String, default: '' },
    select_education: { type: String, default: '' },
    select_experience: { type: String, default: '' }, 
    current_company: { type: String, default: '' },
    job_designation: { type: String, default: '' },
    job_intrested: { type: String, default: '' },
  
}, 
{ 
    timestamps: true,
    collection: "users" 
});

module.exports = mongoose.callforjobs.model('users', users);