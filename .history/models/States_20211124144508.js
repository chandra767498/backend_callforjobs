const mongoose = require("../config/connect");

const States = mongoose.Schema({
    id: { type: Number },
    name: { type: String },
    title: { type: String },
    state_number: { type: Number },
    state_code: { type: String },
    // language_id: { type: Number },
    status: { type: Boolean, default: true }
},
{
    timestamps: true,
    collection: 'states'
});
module.exports = mongoose.callforjobs.model('states', States);