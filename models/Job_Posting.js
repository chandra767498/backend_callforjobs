const mongoose = require("../config/connect");
const { Schema } = require("mongoose");

const Job_Posting = mongoose.Schema({
    id: { type: Number, unique: true },
    // emp_id: { type: String, default: '' },
    company: { type: String, default: '' },
    title: { type: String, default: '' },
    description: { type: String, default: '' },
    email: { type: String, default: '' },
    job_contact: { type: String, default: '' },
    job_skills: { type: Date, default: new Date() },
    country_id: { type: String, default: '' },
    state_id: { type: String, default: '' },
    city_id: { type: String, default: '' },
    salaryfrom: { type: Number, default: '' },
    salaryto: { type: Number, default: '' },
    peroid: { type: String, default: '' },
    categories: { type: String, default: '' },
    type: { type: String, default: '' },
    poiston: { type: String, default: '' },
    gender: { type: String, default: '' },
    date: { type: Date, default: new Date() },
    education: { type: String, default: '' },
    experience: { type: String, default: '' },
    status: {type:String, default: 'active'}
},
{
    timestamps: true,
    collection: 'job_posting'
});

module.exports = mongoose.callforjobs.model('job_posting', Job_Posting);