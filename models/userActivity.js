const mongoose = require("../config/connect");

let activitySchema = mongoose.Schema({
    device_id: { type: String },
    draft_id: { type: Number },
    login_at: { type: Number, default: Date.now },
    logout_at: { type: Number, default: Date.now },
    created_at: { type: Date },
    updated_at: { type: Date }
},
{
    timestamps: true
})

activitySchema.index({ draft_id: -1 });
activitySchema.index({ created_at: -1 });
activitySchema.index({ login_at: -1 });

module.exports = mongoose.callforjobs.model('user_activities', activitySchema);