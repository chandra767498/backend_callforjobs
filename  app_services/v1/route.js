const express=require('express');
const router=express.Router()
const userController=require('./controller/userController')
const DeviceController=require('./controller/DeviceController')
const UserMediator=require('./mediators/userMediator')
const DeviceMediator= require('./mediators/DeviceMediator')
//splash_screen
router.post('/Splash_Screen', DeviceController.Splash_Screen);

router.post('/Splash_Screen_Animation', DeviceMediator.Splash_Screen_Animation);

router.post('/disable_update_menu', DeviceController.Disable_Update_Menu);

//languages
router.get('/List_All_Languages', UserMediator.List_All_Languages);

router.post('/Update_User_Language', UserMediator.Update_User_Language);

//otp
router.post('/Generate_User_OTP', UserMediator.Generate_User_OTP);

router.post('/Validate_User_OTP', UserMediator.Validate_User_OTP);

//user
router.post('/Register',UserMediator.Register);

//static data
router.post('/get_static_data', UserMediator.Get_Static_Data);

module.exports=router;

