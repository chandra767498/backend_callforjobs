let UserMediator = function () {};
const UserController = require("../controller/userController");
const CommonController = require("../../../helpers/CommonController");
const userController = require("../controller/userController");

UserMediator.List_All_Languages = async (req, res) => {
  
  try {
      let Result = await UserController.List_All_Languages();
      return res.send(Result);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(Result)) });
  } 
  catch (error) {
      console.log(error)
      if (!res.headersent) {
          let errorResponse = await CommonController.Common_Error_Handler(error);
          return res.send(errorResponse);
          // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(errorResponse)) });
      }
  }
}


UserMediator.Update_User_Language = async (req, res) => {
  try {
    // if( !req.body.data ){
    //     return res.send({ data: await EncDecNode.encrypt(JSON.stringify({ success: false, msg: "Data is required." })) });
    // }

    // let body = await EncDecNode.decrypt( req.body.data );
    let body = req.body;

    let failResponse = {},
      failStatus = 0;
    if (!body.draft_id) {
      failStatus = 1;
      failResponse = { success: false, msg: " draft id is required." };
    }
    if (!body.language_id) {
      failStatus = 1;
      failResponse = { success: false, msg: "Language id is required." };
    }

    if (failStatus) {
      return res.send(failResponse);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(failResponse)) });
    }

    let Result = await UserController.Update_User_Language(body);
    return res.send(Result);
    // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(Result)) });
  } catch (error) {
    console.log(error);
    if (!res.headersSent) {
      let errorResponse = await CommonController.Common_Error_Handler(error);
      return res.send(errorResponse);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(errorResponse)) });
    }
  }
};

UserMediator.Generate_User_OTP = async (req, res) => {
  try {
      if (
          req.body.draft_id 
          && req.body.country_code
          && req.body.mobile_number
      ) {
          let ValidityStatus = await UserController.Check_for_OTP_Count(req.body);
          let Result = await UserController.Generate_User_OTP_Send_Message(req.body);
          res.json(Result);
      } else {
          throw { success: false, extras: { msg: "ENTER ALL TAGS" } };
      }
  } catch (error) {
      if (!res.headersSent) {
          res.json(error);
      }
  }
};

UserMediator.Validate_User_OTP = async (req, res) => {
  try {
      if (
          req.body.draft_id 
          && req.body.country_code
          && req.body.mobile_number
          && req.body.otp && isFinite(req.body.otp)
      ) {
          let ValidityStatus = await UserController.Check_for_User_OTP_Tries_Count(req.body);
          ValidityStatus = await UserController.Validate_User_OTP(req.body);
          let Result = await UserController.Add_Fetch_User_Profile_Information(req.body);
          res.json(Result);
      } else {
          throw { success: false, extras: { msg: "ENTER ALL TAGS" } };
      }
  } catch (error) {
      console.log(error)
      if (!res.headersSent) {
          res.json(error);
      }
  }
}

UserMediator.Get_Static_Data = async (req, res) => {
  try {
    // if( !req.body.data ){
    //     return res.send({ data: await EncDecNode.encrypt(JSON.stringify({ success: false, msg: "Data is required." })) });
    // }

    // let body = await EncDecNode.decrypt( req.body.data );
    let body = req.body;
    let failResponse = {},
      failStatus = 0;
    if (!body.draft_id) {
      failStatus = 1;
      failResponse = { success: false, msg: "Draft id is required." };
    }
    if (!body.language_id) {
      failStatus = 1;
      failResponse = { success: false, msg: "Language id is required." };
    }

    if (failStatus) {
      return res.send(failResponse);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(failResponse)) });
    }

    let Result = await UserController.Get_Static_Data(body);
    return res.send(Result);
    // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(Result)) });
  } catch (error) {
    console.log(error);
    if (!res.headersSent) {
      let errorResponse = await CommonController.Common_Error_Handler(error);
      return res.send(errorResponse);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(errorResponse)) });
    }
  }
};

UserMediator.Register = async (req, res) => {
  try {
    // if( !req.body.data ){
    //     return res.send({ data: await EncDecNode.encrypt(JSON.stringify({ success: false, msg: "Data is required." })) });
    // }

    // let body = await EncDecNode.decrypt( req.body.data );
    let body = req.body;

    let failResponse = {},
      failStatus = 0;
    if (!body.EmailID) {
      failStatus = 1;
      failResponse = { success: false, msg: "Email is required." };
    }
    if (!body.mobile_number) {
      failStatus = 1;
      failResponse = { success: false, msg: "mobile is required." };
    }
  
    ValidityStatus =
      await CommonController.Check_Whether_Employee_Email_Already_Exist(
        req.body
      );
     
    let Result = await UserController.Register(body);
    console.log(Result);
    return;
    // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(Result)) });
    return res.send(Result);
  } catch (error) {
    if (!res.headersSent) {
      let errorResponse = await CommonController.Common_Error_Handler(error);
      // return res.send({ data: await EncDecNode.encrypt(JSON.stringify(errorResponse)) });
      return res.send(errorResponse);
    }
  }
};


module.exports = UserMediator;
