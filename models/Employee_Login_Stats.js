const tzmoment = require('moment-timezone');
const mongoose = require("../config/connect");

const schema = mongoose.Schema({
    employee_id: { type: Number },
    first_post_id: { type: Number },
    last_post_id: { type: Number },
    first_post_time: { type: Number },
    last_post_time: { type: Number },
    login_at: { type: Number, default: Date.now() },
    logout_at: { type: Number, default: Date.now() },
    work_date: { type: Number, default: tzmoment(new Date()).tz('asia/kolkata').startOf('day').utc().format('x') }
},
{
    timestamps: true,
    collection: 'employee_login_stats'
});

module.exports = mongoose.callforjobs.model('employee_login_stats', schema);