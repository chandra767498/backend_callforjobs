let AdminMediator = function() {};
const AdminController = require("../controller/AdminController");
const CommonController = require("../../helpers/CommonController");
const { isBoolean, Boolify } = require("node-boolify");
const { compile } = require("morgan");

//login
AdminMediator.Login = async(req, res) => {
    try {
        // console.log(req.body)
        if (
            req.body.email_id != null && req.body.email_id != '' &&
            req.body.pwd != null && req.body.pwd != ''
        ) {
           // let ValidityStatus = CommonController.Common_Email_Validation(req.body.email_id);
          
            let AdminData = await AdminController.Check_Whether_Admin_Email_Registered(req.body);
            let Result = await AdminController.Login(req.body, AdminData);
            res.json(Result);
           
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Logout = async(req, res) => {
    try {
        if (!req.body.admin_id) {
            return res.send({ success: false, msg: "Admin id is required." });
        }
        if (!req.body.session_id) {
            return res.send({ success: false, msg: "Session id is required." });
        }
        if (req.body.logout_type != 0 && req.body.logout_type != 1) {
            return res.send({ success: false, msg: "Logout type is required." });
        }
        // if( !req.body.last_event ){
        //     return res.send({ success: false, msg: "Last event is required." });
        // }

        let AdminData = await CommonController.Check_for_Admin(req.body);
        let Result = await AdminController.Logout(req.body);
        return res.json(Result);
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Create_Admin_User = async(req, res) => {
    try {
        if (
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.name != null && req.body.name != '' &&
            req.body.email_id != null && req.body.email_id != '' &&
            req.body.pwd != null && req.body.pwd != '' &&
            req.body.is_admin != null && isBoolean(req.body.is_admin) &&
            req.body.permissions != null && typeof req.body.permissions === "object" 
            // req.body.language_id && req.body.role
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await CommonController.Common_Email_Validation(req.body.email_id);
            ValidityStatus = await AdminController.Check_Whether_Admin_Email_Already_Exist(req.body);
            let Result = await AdminController.Create_Admin_User(req.body, AdminData);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

//language
AdminMediator.Add_Language = async(req, res) => {
   
    try {
        if (
            // req.body.AdminID && req.body.SessionID
            req.body.admin_id && req.body.session_id &&
            req.body.title && req.body.title.trim() &&
            req.body.name && req.body.name.trim() &&
            req.body.type
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_Language(req.body, AdminData);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersent) {
            res.json(error);
        }
    }
}


AdminMediator.List_All_Languages = async(req, res) => {
    try {
        if (
            // req.body.AdminID && req.body.SessionID
            req.body.admin_id && req.body.session_id
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Languages(req.body);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersent) {
            res.json(error);
        }
    }
}


AdminMediator.Update_Language = async(req, res) => {
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            req.body.language_id &&
            req.body.title && req.body.title.trim() &&
            req.body.name && req.body.name.trim()
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (AdminData.is_admin) {
                let Result = await AdminController.Update_Language(req.body);
                res.json(Result);
            } else {
                res.json({ success: false, msg: "Access Denied" });
            }
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
AdminMediator.Add_Static_Language = async(req, res) => {
    try {
        if (
            // req.body.AdminID != null && req.body.SessionID != null
            // && req.body.Title != null && req.body.Title != ''
            // && req.body.SNo != null && isFinite(req.body.SNo)
            // && req.body.Language_Title_Array != null && typeof req.body.Language_Title_Array === "object"
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.language_id != null && req.body.language_id != '' &&
            req.body.title && req.body.title.trim() &&
            req.body.name && req.body.name.trim()
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_Static_Language(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Admin_Fetch_Static_Data = async(req, res) => {
    try {
        if (
            // req.body.AdminID != null && req.body.SessionID != null
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.language_id != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Admin_Fetch_Static_Data(req.body, AdminData);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

//Jobs
AdminMediator.Add_job_category = async(req, res) => {
   
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            req.body.name && req.body.name.trim() &&
            req.body.type
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_job_category(req.body, AdminData);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersent) {
            res.json(error);
        }
    }
}


AdminMediator.List_All_job_Category = async(req, res) => {
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            req.body.skip != null && isFinite(req.body.skip) &&
            req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_job_Category(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Delete_job_category = async(req, res) => {
    try {
        if (
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.category_id !=null
            // req.body.status
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Delete_job_category(req.body, AdminData);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Delete_company = async(req, res) => {
    try {
        if (
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.company_id !=null
            // req.body.status
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Delete_company(req.body, AdminData);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
AdminMediator.List_All_Companies = async(req, res) => {
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            req.body.skip != null && isFinite(req.body.skip) &&
            req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Companies(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_company = async(req, res) => {
   
    try {
       console.log(req.body)
        if (req.body.admin_id && req.body.session_id && req.body.name && req.body.category && req.body.mobile && req.body.url ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_company(req.body, AdminData);
            res.json(Result);
        } else {
            throw { sucess: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersent) {
            res.json(error);
        }
    }
}

//Employees
AdminMediator.List_All_Employees = async(req, res) => {
    try {
        if (
            // req.body.AdminID != null && req.body.SessionID != null
            req.body.admin_id && req.body.session_id &&
            req.body.skip != null && isFinite(req.body.skip) &&
            req.body.limit != null && isFinite(req.body.limit)
            // && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Employees(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Employee = async(req, res) => {
    try {
        if (
           
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.employee_id &&
            req.body.name != null && req.body.name != '' &&
            req.body.email_id &&
            req.body.permissions != null && typeof req.body.permissions === "object"
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Edit_Employee(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Employee = async(req, res) => {
    try {
        console.log(req.body)
        if (
            // req.body.AdminID != null && req.body.SessionID != null
            // && req.body.XAdminID != null
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.employee_id != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Employee(req.body, AdminData);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}




//Postings
AdminMediator.Add_Job_Posting = async(req, res) => {
    try {
        if (!req.body.admin_id) {
            return res.send({ success: false, msg: "Admin id required." })
        }
        if (!req.body.session_id) {
            return res.send({ success: false, msg: "Session id required." })
        }
        // if (!req.body.emp_id) {
        //     return res.send({ success: false, msg: "Employee id required." })
        // }
        // if (!req.body.name) {
        //     return res.send({ success: false, msg: "Name required." })
        // }
        // if (!req.body.gender) {
        //     return res.send({ success: false, msg: "Gender required." })
        // }
        // if (!req.body.designation) {
        //     return res.send({ success: false, msg: "Designation required." })
        // }
        // if (!req.body.location) {
        //     return res.send({ success: false, msg: "Location required." })
        // }
        // if (!req.body.doj) {
        //     return res.send({ success: false, msg: "DOJ required." })
        // }
        // if (!req.body.email_id) {
        //     return res.send({ success: false, msg: "Email id required." })
        // }
        // if (!req.body.mobile_number) {
        //     return res.send({ success: false, msg: "Mobile number required." })
        // }
        // if (!req.body.uan_number) {
        //     return res.send({ success: false, msg: "UAN number required." })
        // }
        // if (!req.body.esic_number) {
        //     return res.send({ success: false, msg: "ESIC number required." })
        // }
        // if (!req.body.bank_account_number) {
        //     return res.send({ success: false, msg: "Bank account number required." })
        // }
        // if (!req.body.ifsc_code) {
        //     return res.send({ success: false, msg: "IFSC code required." })
        // }
        // if (!req.body.branch) {
        //     return res.send({ success: false, msg: "Branch required." })
        // }
        // if (req.body.annual_ctc < 0) {
        //     return res.send({ success: false, msg: "Annual CTC should not be negative." })
        // }
        // if (req.body.current_ctc < 0) {
        //     return res.send({ success: false, msg: "Current CTC should not be negative." })
        // }
        // if (req.body.gross_amount < 0) {
        //     return res.send({ success: false, msg: "Gross amount should not be negative." })
        // }
        // if (req.body.nth < 0) {
        //     return res.send({ success: false, msg: "NTH should not be negative." })
        // }

        let AdminData = await CommonController.Check_for_Admin(req.body);
        let Result = await AdminController.Add_Job_Posting(req.body);
        return res.json(Result);
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


AdminMediator.Active_Inactive_job_posting = async(req, res) => {
    try {
        console.log(req.body)
        if (
            // req.body.AdminID != null && req.body.SessionID != null
            // && req.body.XAdminID != null
            req.body.admin_id != null && req.body.session_id != null &&
            req.body.posting_id != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_job_posting(req.body, AdminData);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
AdminMediator.Get_Job_Posting_Details= async(req, res) => {
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            req.body.skip != null && isFinite(req.body.skip) &&
            req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Job_Posting_Details(req.body);
            return res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
AdminMediator.view_states = async(req, res) => {
    try {
        if (req.body.admin_id != null && req.body.session_id != null) {
            let Result = await AdminController.view_states(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

//Cities
AdminMediator.view_cities = async(req, res) => {
    try {
        if (
            req.body.admin_id && req.body.session_id &&
            (req.body.hasOwnProperty("state_id") || req.body.hasOwnProperty("language_id"))
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.view_cities(req.body);
            res.json(Result);
        } else {
            throw { success: false, msg: "ENTER ALL TAGS" };
        }
    } catch (error) {
        console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
module.exports = AdminMediator;
