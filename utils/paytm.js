let Paytm = module.exports;

const { resolve } = require('path');
let request = require('request');

let credentials = require("../credentials/testMail");
const PaytmChecksum = require('./PaytmChecksum');

let hostName = "https://staging-dashboard.paytm.com";
let mid = credentials.paytmDevMid;
let key = credentials.paytmDevKey;
let subwalletGuid = credentials.subwalletGuidDev;
if( process.env.NODE_ENV == "production" ){
    hostName = "https://dashboard.paytm.com";
    subwalletGuid = credentials.subwalletGuidProd;
    mid = credentials.paytmProdMid;
    key = credentials.patymProdKey;
}

Paytm.SendMoneyToWallet = (data) => {
    return new Promise((resolve)=>{
        let paytmParams = {
            "subwalletGuid": subwalletGuid,
            "orderId": data.orderId,
            "beneficiaryPhoneNo": data.mobile_number,
            "amount": data.amount,
            // "callbackUrl": "https://xapi.anynews.co.in/webhook/paytm"
        }
        // paytmParams["subwalletGuid"]      = "fecfa468-24d2-11eb-be27-fa163e429e83";
        // paytmParams["orderId"]            = "ORDERID_rgukt125";
        // paytmParams["beneficiaryPhoneNo"] = "5555566666";
        // paytmParams["amount"]             = "1.00";
        
        let post_data = JSON.stringify(paytmParams);
        
        /*
        * Generate checksum by parameters we have in body
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
        PaytmChecksum.generateSignature(post_data, key).then(function(checksum){
        
            let x_mid      = mid;
            let x_checksum = checksum;
        
            let options = {
                /* Solutions offered are: food, gift, gratification, loyalty, allowance, communication */
                url: hostName + '/bpay/api/v1/disburse/order/wallet/gratification',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-mid': x_mid,
                    'x-checksum': x_checksum,
                    'Content-Length': post_data.length
                },
                body: post_data
            };
            console.log(options,"options");
        
            request(options, function (error, response, body) {
                if (error) {
                    console.log('error', error);
                    return resolve({ status: false, error: error });
                }
                let resObj = JSON.parse(body);
                console.log("money sending....", resObj)
                resolve({ status: true, data: resObj });
            })
        });
    })
}

Paytm.GetOrderDetails = (data) => {
    return new Promise((resolve)=>{
        let paytmParams = {
            "orderId": data.orderId
        };
    
        // paytmParams["orderId"] = "ORDERID_rgukt125";
    
        let post_data = JSON.stringify(paytmParams);
    
        PaytmChecksum.generateSignature(post_data, key).then(function(checksum){
    
            let x_mid      = mid;
            let x_checksum = checksum;
    
            let options = {
                url: hostName + '/bpay/api/v1/disburse/order/query',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-mid': x_mid,
                    'x-checksum': x_checksum,
                    'Content-Length': post_data.length
                },
                body: post_data
            };
    
            request(options, function (error, response, body) {
                if (error) {
                    console.log('error', error);
                    resolve({ status: false, error: error });
                } else {
                    console.log("verifying", body);
                    let resObj = JSON.parse(body);
                    resolve({ status: true, data: resObj });
                }
            })
        });
    })
}

Paytm.CancelOrder = (data) => {
    return new Promise((resolve)=>{
        let paytmParams = {
            "paytmOrderIds": data.orderIds
        };
    
        // paytmParams["orderId"] = [ "ORDERID_rgukt125" ];
    
        let post_data = JSON.stringify(paytmParams);
    
        PaytmChecksum.generateSignature(post_data, key).then(function(checksum){
    
            let x_mid      = mid;
            let x_checksum = checksum;
    
            let options = {
                url: hostName + '/bpay/api/v1/disburse/order/cancel',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-mid': x_mid,
                    'x-checksum': x_checksum,
                    'Content-Length': post_data.length
                },
                body: post_data
            };
    
            request(options, function (error, response, body) {
                if (error) {
                    console.log('error', error);
                    resolve({ status: false, error: error });
                } else {
                    console.log(response.statusCode, body);
                    let resObj = JSON.parse(body);
                    resolve({ status: true, data: resObj });
                }
            })
        });
    })
}