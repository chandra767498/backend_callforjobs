let config = function() {};

const dotenv = require("dotenv").config();

//Body Parser Limit
config.BodyParserLimit = '50mb';
config.ParameterLimit = 100000000000;


//User OTP Tries and OTP Request
config.OTP_COUNT = 100;
config.OTP_TRIES_COUNT = 100;
//config.PIN_TRIES_COUNT = 10;
config.OTP_COUNT_TIME_IN_MINUTES = 30
config.OTP_TRIES_COUNT_TIME_IN_MINUTES = 30

//MSG91 Credentials
// config.msg91 = {
//     "authkey": "305766AbFfsYat85ddd2200", //anynews
//     "sender_id": "ANYNEW",
//     "route_no": "4"
// }
//FireBase Credentials
// config.firebase = {
//     serverkey: "AAAAQ8xixTk:APA91bFxw7SKirlbkUuowxF1JLd7vzuKnovlcvRdWiCy5drzpDw2HT10W2QpcDj2c3Y-Sl0TSN0WhcrfqvqO1saxEFZbmq-nnIxdOl9LobVjro_QHKymLlOA7EaIOVdpSf19Tg-WJLdG",
//     webApiKey: "AIzaSyC0j4YdWm2eO-rnAUnUBRHOLXRIuov8Khc"
// };


config.msg91 = {
    "authkey": "305766AKJuXk5Q600836d9P1", //OTP
    "sender_id": "HRNPRM",
    "route_no": "4"
}
config.statsMsg = {
    "authkey": "305766A2l55MRc6008495aP1", //STATS
    "sender_id": "HRPRMD",
    "route_no": "4"
}
config.base_url = "https://xadmin.callforjobs.in/";

if (process.env.NODE_ENV === "production") {
    // Production Settings
    config.hostname = 'https://admin.callforjobs.in/';
    config.base_url = "https://admin.callforjobs.in/";
    config.MongoURL = `mongodb://localhost:27017/callforjobs`;
    // config.MongoURL = `mongodb://userevoncallforjobs:userevoncallforjobs123@db.callforjobs.co.in:1234/callforjobs?replicaSet=callforjobs`;
    //config.MongoURL = `mongodb://callforjobs:callforjobs123@ds249008.mlab.com:49008/callforjobs`;
} else {
    console.log('Development Settings')
    config.hostname = 'https://xadmin.callforjobs.in/';
    config.base_url = "https://xadmin.callforjobs.in/";
    // config.MongoURL = `mongodb://userevoncallforjobs:userevoncallforjobs123@xdb.callforjobs.co.in:1234/callforjobs`;

    config.MongoURL = `mongodb://localhost:27017/callforjobs`;
}

module.exports = config;