const mongoose = require("../config/connect");
const Schema = mongoose.Schema;

const Admins = mongoose.Schema({
    admin_id: { type: String, default: "" },
    // session_id: { type: String, default: "" },
    // Name: { type: String, default: "" },
    // EmailID: { type: String, default: "" },
    PasswordHash: { type: String, default: "" },
    PasswordSalt: { type: String, default: "" },
    Whether_God: { type: Boolean, default: false },
    Whether_Employee: { type: Boolean, default: false },
    // language_id: { type: Number, default: 1 },
    // language_name: { type: String, default: "English"},
    Employee_Permissions: {
        
    },
    Permissions: {
        Admin_Section_Permision: { type: Boolean, default: false },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null },

    id: { type: Number, unique: true },
    session_id: { type: String, default: "" },
    name: { type: String, default: "" },
    email_id: { type: String, default: "" },
    // profile_pic: { type: String, default: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp" },
    // original_profile_pic: { type: String, default: "https://newsany.s3-us-west-2.amazonaws.com/user_profile.webp" },
    pwd_hash: { type: String, default: "" },
    pwd_salt: { type: String, default: "" },
    is_admin: { type: Boolean, default: false },
    // language_id: { type: Number, default: 1 },
    permissions: Schema.Types.Mixed,
    status: { type: Boolean, default: true },
    state_id: { type: Number, default: 0 },
    dist_id: { type: Number, default: 0 },
    // role: { type: String, default: "Reporter" }
}, 
{
    timestamps: true,
    collection: 'Admins'
});

module.exports = mongoose.callforjobs.model('Admins', Admins); 